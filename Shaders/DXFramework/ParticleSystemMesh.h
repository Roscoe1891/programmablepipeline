// Tessellation Mesh, point list for tessellation
#ifndef _PARTICLESYSTEMMESH_H_
#define _PARTICLESYSTEMMESH_H_

#include "BaseMesh.h"

using namespace DirectX;

class ParticleSystemMesh : public BaseMesh
{

public:
#define P_SYSTEMSIZE 100

	ParticleSystemMesh(ID3D11Device* device, ID3D11DeviceContext* deviceContext, WCHAR* textureFilename);
	~ParticleSystemMesh();

	void SendData(ID3D11DeviceContext*) const;

protected:
	void InitBuffers(ID3D11Device* device);


};

#endif