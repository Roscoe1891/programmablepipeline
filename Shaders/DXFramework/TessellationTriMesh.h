// Tessellation Mesh, point list for tessellation
#ifndef _TESSELLATIONTRIMESH_H_
#define _TESSELLATIONTRIMESH_H_

#include "BaseMesh.h"

using namespace DirectX;

class TessellationTriMesh : public BaseMesh
{

public:
	TessellationTriMesh(ID3D11Device* device, ID3D11DeviceContext* deviceContext, WCHAR* textureFilename);
	~TessellationTriMesh();

	void SendData(ID3D11DeviceContext*);

protected:
	void InitBuffers(ID3D11Device* device);
	
};

#endif