// Tessellation Mesh, point list for tessellation
#ifndef _TESSELLATIONQUADMESH_H_
#define _TESSELLATIONQUADMESH_H_

#include "BaseMesh.h"

using namespace DirectX;

class TessellationQuadMesh : public BaseMesh
{

public:
	TessellationQuadMesh(ID3D11Device* device, ID3D11DeviceContext* deviceContext, WCHAR* textureFilename); //, WCHAR* heightMapFilename);
	~TessellationQuadMesh();

	void SendData(ID3D11DeviceContext*) const;

	//ID3D11ShaderResourceView* GetHeightMap() const;

protected:
	void InitBuffers(ID3D11Device* device);
	//void LoadHeightMap(ID3D11Device* device, ID3D11DeviceContext* deviceContext, WCHAR* fileName);

	//Texture* m_heightMap;
	
};

#endif