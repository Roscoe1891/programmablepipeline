#include "App1.h"

App1::App1()
{
	//BaseApplication::BaseApplication();
	m_CubeMesh = nullptr;
	m_PlaneMesh = nullptr;
	m_ParticleSystemMesh = nullptr;
	m_TesselationMesh = nullptr;
	m_diamondModel = nullptr;
	m_pillarModel = nullptr;

	m_RenderTextureShadow = nullptr;
	m_RenderTextureDepth = nullptr;
	m_RenderTextureScene = nullptr;
	m_RenderTextureHorizontalBlur = nullptr;
	m_RenderTextureVerticalBlur = nullptr;

	m_OrthoMeshShadow = nullptr;
	m_OrthoMeshDepth = nullptr;
	m_OrthoMeshScene = nullptr;
	m_OrthoMeshHorizontalBlur = nullptr;
	m_OrthoMeshVerticalBlur = nullptr;
	m_OrthoMeshPostProcessed = nullptr;

	m_OrthoMeshHorizontalBlurFullWindow = nullptr;
	m_OrthoMeshVerticalBlurFullWindow = nullptr;

}

App1::~App1()
{
	// Run base application deconstructor
	BaseApplication::~BaseApplication();

	// Clean up
	CleanUp();
}

void App1::init(HINSTANCE hinstance, HWND hwnd, int screenWidth, int screenHeight, Input *in)
{
	// Call super init function (required!)
	BaseApplication::init(hinstance, hwnd, screenWidth, screenHeight, in);

	// Create Lights
	m_LightManager.InitLights();

	// Create Shaders
	m_ShaderManager.CreateShaders(m_Direct3D->GetDevice(), m_Direct3D->GetDeviceContext(), hwnd);
	
	// Create Render Textures
	CreateRenderTextures();

	// Create Mesh objects
	CreateMeshes();
	
	// Init SceneObjects
	InitSceneObjects();

	// Init GUI
	m_GUI.Init(&m_ShaderManager, &m_LightManager, &m_ParticleSystem);
	
	// Set initial camera start and rotation
	m_Camera->SetPosition(40.f, 14.f, 20.f);
	m_Camera->SetRotation(29.f, 218.f, 0.f);

}

void App1::CreateRenderTextures()
{
	m_RenderTextureShadow = new RenderTexture(m_Direct3D->GetDevice(), 1024, 1024, SCREEN_NEAR, SCREEN_DEPTH);
	m_RenderTextureDepth = new RenderTexture(m_Direct3D->GetDevice(), 1024, 1024, SCREEN_NEAR, SCREEN_DEPTH);
	m_RenderTextureScene = new RenderTexture(m_Direct3D->GetDevice(), 1024, 1024, SCREEN_NEAR, SCREEN_DEPTH);
	m_RenderTextureHorizontalBlur = new RenderTexture(m_Direct3D->GetDevice(), 1024, 1024, SCREEN_NEAR, SCREEN_DEPTH);
	m_RenderTextureVerticalBlur = new RenderTexture(m_Direct3D->GetDevice(), 1024, 1024, SCREEN_NEAR, SCREEN_DEPTH);
}

void App1::CreateMeshes()
{
	m_CubeMesh = new CubeMesh(m_Direct3D->GetDevice(), m_Direct3D->GetDeviceContext(), L"../res/brick_brown.jpg");
	m_PlaneMesh = new PlaneMesh(m_Direct3D->GetDevice(), m_Direct3D->GetDeviceContext(), L"../res/water.jpg");
	m_ParticleSystemMesh = new ParticleSystemMesh(m_Direct3D->GetDevice(), m_Direct3D->GetDeviceContext(), L"../res/particle.png");
	m_TesselationMesh = new TessellationQuadMesh(m_Direct3D->GetDevice(), m_Direct3D->GetDeviceContext(), L"../res/rock.JPG");

	m_diamondModel = new Model(m_Direct3D->GetDevice(), m_Direct3D->GetDeviceContext(), L"../res/grey_stone.jpg", L"../res/diamond.obj");
	m_pillarModel = new Model(m_Direct3D->GetDevice(), m_Direct3D->GetDeviceContext(), L"../res/stone.jpg", L"../res/pillar.obj");

	// Orthomeshes used as part of the rendering process
	m_OrthoMeshPostProcessed = new OrthoMesh(m_Direct3D->GetDevice(), m_Direct3D->GetDeviceContext(), sWidth, sHeight, 0, 0);
	m_OrthoMeshHorizontalBlurFullWindow = new OrthoMesh(m_Direct3D->GetDevice(), m_Direct3D->GetDeviceContext(), sWidth, sHeight, 0, 0);
	m_OrthoMeshVerticalBlurFullWindow = new OrthoMesh(m_Direct3D->GetDevice(), m_Direct3D->GetDeviceContext(), sWidth, sHeight, 0, 0);

	// Orthomeshes for debug purposes
	m_OrthoMeshShadow = new OrthoMesh(m_Direct3D->GetDevice(), m_Direct3D->GetDeviceContext(), 200, 150, -300, 225);
	m_OrthoMeshDepth = new OrthoMesh(m_Direct3D->GetDevice(), m_Direct3D->GetDeviceContext(), 200, 150, 300, 225);
	m_OrthoMeshScene = new OrthoMesh(m_Direct3D->GetDevice(), m_Direct3D->GetDeviceContext(), 200, 150, 300, 0);
	m_OrthoMeshHorizontalBlur = new OrthoMesh(m_Direct3D->GetDevice(), m_Direct3D->GetDeviceContext(), 200, 150, 300, -225);
	m_OrthoMeshVerticalBlur = new OrthoMesh(m_Direct3D->GetDevice(), m_Direct3D->GetDeviceContext(), 200, 150, 300, 0);
}

void App1::InitSceneObjects()
{
	m_liquid_plane.Init(m_PlaneMesh, &m_ShaderManager, m_Direct3D->GetDeviceContext(), XMFLOAT4(-50.f, -3.f, -50.f, 1.f), XMFLOAT4(0.f, 0.f, 0.f, 1.f), XMFLOAT4(1.f, 1.f, 1.f, 1.f));
	m_platform.Init(m_CubeMesh, &m_ShaderManager, m_Direct3D->GetDeviceContext(), XMFLOAT4(31.f, -2.f, 0.f, 1.f), XMFLOAT4(0.f, 0.f, 0.f, 1.f), XMFLOAT4(10.f, 1.f, 10.f, 1.f));
	m_ParticleSystem.Init(m_ParticleSystemMesh, &m_ShaderManager, m_Direct3D->GetDeviceContext(), XMFLOAT4(28.f, 1.f, 0.f, 1.f), XMFLOAT4(0.f, 0.f, 0.f, 1.f), XMFLOAT4(1.f, 1.f, 1.f, 1.f));
	m_Terrain.Init(m_TesselationMesh, &m_ShaderManager, m_Direct3D->GetDevice(), m_Direct3D->GetDeviceContext(), L"../res/heightmap.jpg",
		XMFLOAT4(0.f, 63.f, 0.f, 1.f), XMFLOAT4((float)PI / 2.f, 0.f, 0.f, 1.f), XMFLOAT4(100.f, 100.f, 100.f, 1.f));

	m_diamond.Init(m_diamondModel, &m_ShaderManager, m_Direct3D->GetDeviceContext(), XMFLOAT4(28.f, 2.f, 0.f, 1.f), XMFLOAT4(0.f, 10.f, 0.f, 1.f), XMFLOAT4(0.025f, 0.025f, 0.025f, 1.f));
	m_diamond.SetRotationSpeed(2.f);
	m_pillar1.Init(m_pillarModel, &m_ShaderManager, m_Direct3D->GetDeviceContext(), XMFLOAT4(30.f, -1.f, -5.f, 1.f), XMFLOAT4(0.f, 0.f, 0.f, 1.f), XMFLOAT4(0.05f, 0.05f, 0.05f, 1.f));
	m_pillar2.Init(m_pillarModel, &m_ShaderManager, m_Direct3D->GetDeviceContext(), XMFLOAT4(30.f, -1.f, 5.f, 1.f), XMFLOAT4(0.f, 0.f, 0.f, 1.f), XMFLOAT4(0.05f, 0.05f, 0.05f, 1.f));

	// draw objects
	m_windowHorizontalBlur.Init(m_OrthoMeshHorizontalBlurFullWindow, &m_ShaderManager, m_Direct3D->GetDeviceContext(), XMFLOAT4(0.f, 0.f, 0.f, 1.f), XMFLOAT4(0.f, 0.f, 0.f, 1.f), XMFLOAT4(1.f, 1.f, 1.f, 1.f));
	m_windowVerticalBlur.Init(m_OrthoMeshVerticalBlurFullWindow, &m_ShaderManager, m_Direct3D->GetDeviceContext(), XMFLOAT4(0.f, 0.f, 0.f, 1.f), XMFLOAT4(0.f, 0.f, 0.f, 1.f), XMFLOAT4(1.f, 1.f, 1.f, 1.f));
	m_finalWindow.Init(m_OrthoMeshPostProcessed, &m_ShaderManager, m_Direct3D->GetDeviceContext(), XMFLOAT4(0.f, 0.f, 0.f, 1.f), XMFLOAT4(0.f, 0.f, 0.f, 1.f), XMFLOAT4(1.f, 1.f, 1.f, 1.f));

	// debug windows
	m_debugWindowShadow.Init(m_OrthoMeshShadow, &m_ShaderManager, m_Direct3D->GetDeviceContext(), XMFLOAT4(0.f, 0.f, 0.f, 1.f), XMFLOAT4(0.f, 0.f, 0.f, 1.f), XMFLOAT4(1.f, 1.f, 1.f, 1.f));
	m_debugWindowDepth.Init(m_OrthoMeshDepth, &m_ShaderManager, m_Direct3D->GetDeviceContext(), XMFLOAT4(0.f, 0.f, 0.f, 1.f), XMFLOAT4(0.f, 0.f, 0.f, 1.f), XMFLOAT4(1.f, 1.f, 1.f, 1.f));
	m_debugWindowScene.Init(m_OrthoMeshScene, &m_ShaderManager, m_Direct3D->GetDeviceContext(), XMFLOAT4(0.f, 0.f, 0.f, 1.f), XMFLOAT4(0.f, 0.f, 0.f, 1.f), XMFLOAT4(1.f, 1.f, 1.f, 1.f));
	m_debugWindowHorizontalBlur.Init(m_OrthoMeshHorizontalBlur, &m_ShaderManager, m_Direct3D->GetDeviceContext(), XMFLOAT4(0.f, 0.f, 0.f, 1.f), XMFLOAT4(0.f, 0.f, 0.f, 1.f), XMFLOAT4(1.f, 1.f, 1.f, 1.f));
	m_debugWindowVerticalBlur.Init(m_OrthoMeshVerticalBlur, &m_ShaderManager, m_Direct3D->GetDeviceContext(), XMFLOAT4(0.f, 0.f, 0.f, 1.f), XMFLOAT4(0.f, 0.f, 0.f, 1.f), XMFLOAT4(1.f, 1.f, 1.f, 1.f));
}


bool App1::Frame()
{
	bool result;

	result = BaseApplication::Frame();
	if (!result)
	{
		return false;
	}

	// Get delta time
	float delta_time = m_Timer->GetTime();

	// update particles
	m_ParticleSystem.Update(delta_time, m_Camera->GetPosition());

	// spin diamond shape
	m_diamond.Rotate(delta_time, 'y');

	// Update shader manager
	m_ShaderManager.Update(delta_time, m_Input, m_Camera->GetPosition(), m_Terrain.GetPositionXMF3());

	// Update GUI
	m_GUI.Update();

	// Render the graphics.
	result = Render();
	if (!result)
	{
		return false;
	}

	return true;
}

bool App1::Render()
{
	// check if UI has enabled wireframe
	if(m_GUI.GetWireFrameEnabled())
		m_Direct3D->TurnOnWireframe();
	else
		m_Direct3D->TurnOffWireframe();

	if (m_ShaderManager.GetBlurLevel() == BlurLevel::OFF)
	{
		// STAGE 1 : GET A DEPTH MAP USED FOR SHADOWS
		if (!RenderToTextureShadow()) return false;

		// STAGE 2 : RENDER FINAL IMAGE
		if (!RenderNoBlur()) return false;
	}
	else
	{
		// STAGE 1 : GET A DEPTH MAP USED FOR SHADOWS
		if (!RenderToTextureShadow()) return false;

		// STAGE 2 : GET A DEPTH MAP FROM THE CAMERA TO CALCULATE BLUR LATER 
		if (!RenderToTextureDepth()) return false;

		// STAGE 3 : RENDER SCENE (WITH SHADOWS) TO TEXTURE
		if (!RenderToTexture()) return false;

		// STAGE 4 : BLUR (USING DEPTH MAP) HORIZONTALLY  
		if (!BlurHorizontal()) return false;

		// STAGE 5 : BLUR (USING DEPTH MAP) VERTICALLY
		if (!BlurVertical()) return false;

		// STAGE 6 : RENDER FINAL POST PROCESS IMAGE
		if (!RenderPostProcess()) return false;
	}

	return true;
}

bool App1::RenderToTextureShadow()
{
	// STAGE 1 : GET A DEPTH MAP USED FOR SHADOWS

	// Set the render target to be the SHADOW render texture.
	m_RenderTextureShadow->SetRenderTarget(m_Direct3D->GetDeviceContext());

	// Clear the render target.
	m_RenderTextureShadow->ClearRenderTarget(m_Direct3D->GetDeviceContext(), 0.f, 0.f, 0.f, 1.f);

	// Generate the view matrix based on the camera's position.
	m_Camera->Update();

	// Get the world matrix from d3d.
	RenderMatrixGroup matrix_group;
	m_Direct3D->GetWorldMatrix(matrix_group.worldMatrix);
	// View and projection matrices must be generated by the light
	// We are only concerned about light 0
	m_LightManager.lights()[0].GenerateViewMatrix();
	m_LightManager.lights()[0].GenerateProjectionMatrix(SCREEN_NEAR, SCREEN_DEPTH);
	matrix_group.viewMatrix = m_LightManager.lights()[0].GetViewMatrix();
	matrix_group.projectionMatrix = m_LightManager.lights()[0].GetProjectionMatrix();

	// Put the model vertex and index buffers on the graphics pipeline to prepare them for drawing.
	// For models that will be shadowed
	m_diamond.Render(ShaderManager::DEPTH, matrix_group);

	m_pillar1.Render(ShaderManager::DEPTH, matrix_group);
	m_pillar2.Render(ShaderManager::DEPTH, matrix_group);

	m_platform.Render(ShaderManager::DEPTH, matrix_group);

	// Reset the render target back to the original back buffer and not the render to texture anymore.
	m_Direct3D->SetBackBufferRenderTarget();
	
	// Reset the viewport
	m_Direct3D->ResetViewport();

	return true;
}

bool App1::RenderToTextureDepth()			
{
	// STAGE 2 : GET A DEPTH MAP FROM THE CAMERA TO CALCULATE BLUR LATER

	// Set the render target to be the DEPTH render texture.
	m_RenderTextureDepth->SetRenderTarget(m_Direct3D->GetDeviceContext());

	// Clear the render target.
	m_RenderTextureDepth->ClearRenderTarget(m_Direct3D->GetDeviceContext(), 0.f, 0.f, 0.f, 1.f);

	// Generate the view matrix based on the camera's position.
	m_Camera->Update();

	// Get the world matrix from d3d.
	RenderMatrixGroup matrix_group;
	//// Get the world, view, projection, and ortho matrices from the camera and Direct3D objects.
	m_Direct3D->GetWorldMatrix(matrix_group.worldMatrix);
	m_Camera->GetViewMatrix(matrix_group.viewMatrix);
	m_Direct3D->GetProjectionMatrix(matrix_group.projectionMatrix);

	// Render all objects
	m_platform.Render(ShaderManager::DEPTH, matrix_group);
	
	m_diamond.Render(ShaderManager::DEPTH, matrix_group);
	
	m_pillar1.Render(ShaderManager::DEPTH, matrix_group);
	m_pillar2.Render(ShaderManager::DEPTH, matrix_group);

	m_liquid_plane.Render(ShaderManager::DEPTH, matrix_group);
	
	// Render 'specialist' depth pass objects
	m_Terrain.Render(ShaderManager::TESSELATION_DEPTH, matrix_group);
	m_ParticleSystem.Render(ShaderManager::PARTICLE_DEPTH, matrix_group);
	
	// Reset the render target back to the original back buffer and not the render to texture anymore.
	m_Direct3D->SetBackBufferRenderTarget();

	// Reset the viewport
	m_Direct3D->ResetViewport();

	return true;
}

bool App1::RenderToTexture()
{
	// STAGE 3 : RENDER SCENE (WITH SHADOWS) TO TEXTURE

	// Set the render target to be the render to texture.
	m_RenderTextureScene->SetRenderTarget(m_Direct3D->GetDeviceContext());

	// Clear the render target.
	m_RenderTextureScene->ClearRenderTarget(m_Direct3D->GetDeviceContext(), 0.2f, 0.5f, 0.75f, 1.f);

	// Generate the view matrix based on the camera's position.
	m_Camera->Update();

	// Get the world matrix from d3d.
	RenderMatrixGroup matrix_group;
	//// Get the world, view, projection, and ortho matrices from the camera and Direct3D objects.
	m_Direct3D->GetWorldMatrix(matrix_group.worldMatrix);
	m_Camera->GetViewMatrix(matrix_group.viewMatrix);
	m_Direct3D->GetProjectionMatrix(matrix_group.projectionMatrix);

	// Render objects...
	// That will be shadowed
	m_platform.Render(ShaderManager::SHADOW, matrix_group, m_RenderTextureShadow->GetShaderResourceView(), m_LightManager.lights(), m_Camera);

	m_diamond.Render(ShaderManager::SHADOW, matrix_group, m_RenderTextureShadow->GetShaderResourceView(), m_LightManager.lights(), m_Camera);

	m_pillar1.Render(ShaderManager::SHADOW, matrix_group, m_RenderTextureShadow->GetShaderResourceView(), m_LightManager.lights(), m_Camera);
	m_pillar2.Render(ShaderManager::SHADOW, matrix_group, m_RenderTextureShadow->GetShaderResourceView(), m_LightManager.lights(), m_Camera);

	// Render liquid plane
	m_liquid_plane.Render(ShaderManager::RIPPLE, matrix_group, m_LightManager.lights(), m_Camera->GetPosition());

	// Render tesselation mesh
	m_Terrain.Render(ShaderManager::TESSELATION, matrix_group, m_LightManager.lights());

	// draw objects which use alpha blending
	m_Direct3D->TurnOnAlphaBlending();

	m_ParticleSystem.Render(ShaderManager::PARTICLE, matrix_group, m_LightManager.lights());

	m_Direct3D->TurnOffAlphaBlending();

	// Reset the render target back to the original back buffer and not the render to texture anymore.
	m_Direct3D->SetBackBufferRenderTarget();

	// Reset the viewport
	 m_Direct3D->ResetViewport();

	return true;
}

bool App1::BlurHorizontal()  
{
	// STAGE 4 : BLUR (USING DEPTH MAP) HORIZONTALLY

	// Set the render target
	m_RenderTextureHorizontalBlur->SetRenderTarget(m_Direct3D->GetDeviceContext());

	// Clear the render target
	m_RenderTextureHorizontalBlur->ClearRenderTarget(m_Direct3D->GetDeviceContext(), 0.f, 0.f, 0.f, 1.f);

	// Generate the view matrix based on the camera's position.
	m_Camera->Update();

	// Turn off the Z buffer to begin all 2D rendering.
	m_Direct3D->TurnZBufferOff();

	// Get orthographic & baseview matrices for 2D rendering
	RenderMatrixGroup matrices;
	m_Direct3D->GetWorldMatrix(matrices.worldMatrix);
	m_Direct3D->GetOrthoMatrix(matrices.projectionMatrix);// ortho matrix for 2D rendering
	m_Camera->GetBaseViewMatrix(matrices.viewMatrix);

	// draw 2D object using the horizontal blur shader
	m_windowHorizontalBlur.Render(ShaderManager::BLUR_HORIZONTAL, matrices, m_RenderTextureScene->GetShaderResourceView(), m_RenderTextureDepth->GetShaderResourceView(), (float)sWidth);

	// Finished 2D rendering, turn Z buffer back on
	m_Direct3D->TurnZBufferOn();

	// Reset the render target back to the original back buffer and not the render to texture anymore.
	m_Direct3D->SetBackBufferRenderTarget();

	// Reset the viewport
	m_Direct3D->ResetViewport();

	return true;
}

bool App1::BlurVertical()
{
	// STAGE 5 : BLUR (USING DEPTH MAP) VERTICALLY

	// Set the render target
	m_RenderTextureVerticalBlur->SetRenderTarget(m_Direct3D->GetDeviceContext());

	// Clear the render target
	m_RenderTextureVerticalBlur->ClearRenderTarget(m_Direct3D->GetDeviceContext(), 0.f, 0.f, 0.f, 1.f);

	// Generate the view matrix based on the camera's position.
	m_Camera->Update();

	// Turn off the Z buffer to begin all 2D rendering.
	m_Direct3D->TurnZBufferOff();

	// Get orthographic & baseview matrices for 2D rendering
	RenderMatrixGroup matrices;
	m_Direct3D->GetWorldMatrix(matrices.worldMatrix);
	m_Direct3D->GetOrthoMatrix(matrices.projectionMatrix);// ortho matrix for 2D rendering
	m_Camera->GetBaseViewMatrix(matrices.viewMatrix);

	// draw 2D object using the horizontal blur shader
	m_windowVerticalBlur.Render(ShaderManager::BLUR_VERTICAL, matrices, m_RenderTextureHorizontalBlur->GetShaderResourceView(), m_RenderTextureDepth->GetShaderResourceView(), (float)sWidth);

	// Finished 2D rendering, turn Z buffer back on
	m_Direct3D->TurnZBufferOn();

	// Reset the render target back to the original back buffer and not the render to texture anymore.
	m_Direct3D->SetBackBufferRenderTarget();

	// Reset the viewport
	m_Direct3D->ResetViewport();

	return true;
}

bool App1::RenderPostProcess()
{
	// STAGE 6 : RENDER FINAL POST PROCESS IMAGE

	// Clear the scene. (default blue colour)
	//m_Direct3D->BeginScene(0.39f, 0.58f, 0.92f, 1.0f);
	m_Direct3D->BeginScene(0.2f, 0.5f, 0.75f, 1.0f);

	// Generate the view matrix based on the camera's position.
	m_Camera->Update();

	// Turn off the Z buffer to begin all 2D rendering.
	m_Direct3D->TurnZBufferOff();

	// Get orthographic & baseview matrices for 2D rendering
	RenderMatrixGroup matrices;
	m_Direct3D->GetWorldMatrix(matrices.worldMatrix);
	m_Direct3D->GetOrthoMatrix(matrices.projectionMatrix);// ortho matrix for 2D rendering
	m_Camera->GetBaseViewMatrix(matrices.viewMatrix);

	// draw 2D object using the horizontal blur shader
	m_finalWindow.Render(ShaderManager::TEXTURE, matrices, m_RenderTextureVerticalBlur->GetShaderResourceView());

	// render 2D objects
	if (m_GUI.GetShowShadowDepthPass()) m_debugWindowShadow.Render(ShaderManager::TEXTURE, matrices, m_RenderTextureShadow->GetShaderResourceView());
	if (m_GUI.GetShowPostProcessStages()) m_debugWindowDepth.Render(ShaderManager::TEXTURE, matrices, m_RenderTextureDepth->GetShaderResourceView());
	if (m_GUI.GetShowPostProcessStages()) m_debugWindowScene.Render(ShaderManager::TEXTURE, matrices, m_RenderTextureScene->GetShaderResourceView());
	if (m_GUI.GetShowPostProcessStages()) m_debugWindowHorizontalBlur.Render(ShaderManager::TEXTURE, matrices, m_RenderTextureHorizontalBlur->GetShaderResourceView());
	//m_debugWindowVerticalBlur.Render(ShaderManager::TEXTURE, matrices, m_RenderTextureVerticalBlur->GetShaderResourceView());
	
	// Draw GUI
	m_GUI.Render(m_Direct3D->GetDeviceContext());

	// Finished 2D rendering, turn Z buffer back on
	m_Direct3D->TurnZBufferOn();

	// Present the rendered scene to the screen.
	m_Direct3D->EndScene();

	return true;
}

bool App1::RenderNoBlur()
{
	// RENDER THE WHOLE SCENE WITH NO BLUR

	//// Clear the scene. (default blue colour)
	m_Direct3D->BeginScene(0.2f, 0.5f, 0.75f, 1.0f);

	//// Generate the view matrix based on the camera's position.
	m_Camera->Update();

	// Create a matrix group to hold our matrices
	RenderMatrixGroup matrix_group;

	//// Get the world, view, projection, and ortho matrices from the camera and Direct3D objects.
	m_Direct3D->GetWorldMatrix(matrix_group.worldMatrix);
	m_Camera->GetViewMatrix(matrix_group.viewMatrix);
	m_Direct3D->GetProjectionMatrix(matrix_group.projectionMatrix);

	// Render objects...
	// That will be shadowed
	m_diamond.Render(ShaderManager::SHADOW, matrix_group, m_RenderTextureShadow->GetShaderResourceView(), m_LightManager.lights(), m_Camera);

	m_pillar1.Render(ShaderManager::SHADOW, matrix_group, m_RenderTextureShadow->GetShaderResourceView(), m_LightManager.lights(), m_Camera);
	m_pillar2.Render(ShaderManager::SHADOW, matrix_group, m_RenderTextureShadow->GetShaderResourceView(), m_LightManager.lights(), m_Camera);


	m_platform.Render(ShaderManager::SHADOW, matrix_group, m_RenderTextureShadow->GetShaderResourceView(), m_LightManager.lights(), m_Camera);
	
	// liquid plane doesn't get shadows
	m_liquid_plane.Render(ShaderManager::RIPPLE, matrix_group, m_LightManager.lights(), m_Camera->GetPosition());

	// Render tesselation mesh
	m_Terrain.Render(ShaderManager::TESSELATION, matrix_group, m_LightManager.lights());

	// draw objects which use alpha b;lending
	m_Direct3D->TurnOnAlphaBlending();

	m_ParticleSystem.Render(ShaderManager::PARTICLE, matrix_group, m_LightManager.lights());

	m_Direct3D->TurnOffAlphaBlending();

	
	// Render the ortho mesh
	// Turn off the Z buffer to begin all 2D rendering.
	m_Direct3D->TurnZBufferOff();

	// Get orthographic & baseview matrices for 2D rendering
	RenderMatrixGroup matrices;
	m_Direct3D->GetWorldMatrix(matrices.worldMatrix);
	m_Direct3D->GetOrthoMatrix(matrices.projectionMatrix);// ortho matrix for 2D rendering
	m_Camera->GetBaseViewMatrix(matrices.viewMatrix);

	// render 2D objects
	// show depth pass if this has been enabled
	if (m_GUI.GetShowShadowDepthPass()) m_debugWindowShadow.Render(ShaderManager::TEXTURE, matrices, m_RenderTextureShadow->GetShaderResourceView());

	// Draw GUI
	m_GUI.Render(m_Direct3D->GetDeviceContext());

	// Finished 2D rendering, turn Z buffer back on
	m_Direct3D->TurnZBufferOn();

	// Present the rendered scene to the screen.
	m_Direct3D->EndScene();

	return true;
}

void App1::CleanUp()
{
	// Release the Direct3D objects.
	if (m_CubeMesh)
	{
		delete m_CubeMesh;
		m_CubeMesh = nullptr;
	}

	if (m_PlaneMesh)
	{
		delete m_PlaneMesh;
		m_PlaneMesh = nullptr;
	}

	if (m_ParticleSystemMesh)
	{
		delete m_ParticleSystemMesh;
		m_ParticleSystemMesh = nullptr;
	}

	if (m_TesselationMesh)
	{
		delete m_TesselationMesh;
		m_TesselationMesh = nullptr;
	}

	if (m_diamondModel)
	{
		delete m_diamondModel;
		m_diamondModel = nullptr;
	}

	if (m_pillarModel)
	{
		delete m_pillarModel;
		m_pillarModel = nullptr;
	}

	// OrthoMeshes
	if (m_OrthoMeshShadow)
	{
		delete m_OrthoMeshShadow;
		m_OrthoMeshShadow = nullptr;
	}

	if (m_OrthoMeshDepth)
	{
		delete  m_OrthoMeshDepth;
		m_OrthoMeshDepth = nullptr;
	}

	if (m_OrthoMeshScene)
	{
		delete m_OrthoMeshScene;
		m_OrthoMeshScene = nullptr;
	}

	if (m_OrthoMeshHorizontalBlur)
	{
		delete m_OrthoMeshHorizontalBlur;
		m_OrthoMeshHorizontalBlur = nullptr;
	}

	if (m_OrthoMeshVerticalBlur)
	{
		delete m_OrthoMeshVerticalBlur;
		m_OrthoMeshVerticalBlur = nullptr;
	}

	if (m_OrthoMeshPostProcessed)
	{
		delete m_OrthoMeshPostProcessed;
		m_OrthoMeshPostProcessed = nullptr;
	}

	if (m_OrthoMeshHorizontalBlurFullWindow)
	{
		delete m_OrthoMeshHorizontalBlurFullWindow;
		m_OrthoMeshHorizontalBlurFullWindow = nullptr;
	}

	if (m_OrthoMeshVerticalBlurFullWindow)
	{
		delete m_OrthoMeshVerticalBlurFullWindow;
		m_OrthoMeshVerticalBlurFullWindow = nullptr;
	}

	// Release RenderTextures
	if (m_RenderTextureShadow)
	{
		delete m_RenderTextureShadow;
		m_RenderTextureShadow = nullptr;
	}

	if (m_RenderTextureDepth)
	{
		delete m_RenderTextureDepth;
		m_RenderTextureDepth = nullptr;
	}

	if (m_RenderTextureScene)
	{
		delete m_RenderTextureScene;
		m_RenderTextureScene = nullptr;
	}

	if (m_RenderTextureHorizontalBlur)
	{
		delete m_RenderTextureHorizontalBlur;
		m_RenderTextureHorizontalBlur = nullptr;
	}

	if (m_RenderTextureVerticalBlur)
	{
		delete m_RenderTextureVerticalBlur;
		m_RenderTextureVerticalBlur = nullptr;
	}

}

