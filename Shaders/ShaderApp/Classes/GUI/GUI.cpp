#include "GUI.h"

namespace ShaderApp
{

	GUI::GUI()
	{
		m_ShaderManager = nullptr;
		m_LightManager = nullptr;
	}

	GUI::~GUI()
	{
	}

	void GUI::Init(ShaderManager* shaderManager, LightManager* lightManager, ParticleSystem* particleSystem)
	{
		m_ShaderManager = shaderManager;
		m_LightManager = lightManager;
		m_ParticleSystem = particleSystem;

		m_wireFrameEnabled = false;
		m_showShadowDepthPass = false;
		m_showPostProcessStages = false;

		// lights
		m_light1On = m_LightManager->lights()[1].GetOn();
		m_light1Colour = ImVec4(m_LightManager->lights()[1].GetDiffuseColour().x, m_LightManager->lights()[1].GetDiffuseColour().y, m_LightManager->lights()[1].GetDiffuseColour().z, 1.f);
		m_light2On = m_LightManager->lights()[2].GetOn();
		m_light2Colour = ImVec4(m_LightManager->lights()[2].GetDiffuseColour().x, m_LightManager->lights()[2].GetDiffuseColour().y, m_LightManager->lights()[2].GetDiffuseColour().z, 1.f);
		m_light3On = m_LightManager->lights()[3].GetOn();
		m_light3Colour = ImVec4(m_LightManager->lights()[3].GetDiffuseColour().x, m_LightManager->lights()[3].GetDiffuseColour().y, m_LightManager->lights()[3].GetDiffuseColour().z, 1.f);

		// wave
		m_wavesHeight = m_ShaderManager->GetRippleShader()->GetWaveHeight();
		m_wavesFrequency = m_ShaderManager->GetRippleShader()->GetWaveHeight();

		// terrain tesselation
		m_tesselationFactor = m_ShaderManager->GetTesselationShader()->GetTesselationFactor();
		m_tesselationEditsEnabled = m_ShaderManager->GetTesselationShader()->GetCameraBasedTesselationDisabled();

		// particles
		m_particlePositionVariance[0] = m_ParticleSystem->GetPositionVariance().x;
		m_particlePositionVariance[1] = m_ParticleSystem->GetPositionVariance().y;
		m_particlePositionVariance[2] = m_ParticleSystem->GetPositionVariance().z;
		
		m_particleMinimumVelocity = m_ParticleSystem->GetMinimumSpawnVelocity().y;
		
		m_particleMinimumLifespan = m_ParticleSystem->GetMinimumLifespan();

		// blur
		m_blurLevel = m_ShaderManager->GetBlurLevel();
	}

	void GUI::Update()
	{

		// lights
		m_light1On = m_LightManager->lights()[1].GetOn();
		m_light1Colour = ImVec4(m_LightManager->lights()[1].GetDiffuseColour().x, m_LightManager->lights()[1].GetDiffuseColour().y, m_LightManager->lights()[1].GetDiffuseColour().z, 1.f);
		m_light2On = m_LightManager->lights()[2].GetOn();
		m_light2Colour = ImVec4(m_LightManager->lights()[2].GetDiffuseColour().x, m_LightManager->lights()[2].GetDiffuseColour().y, m_LightManager->lights()[2].GetDiffuseColour().z, 1.f);
		m_light3On = m_LightManager->lights()[3].GetOn();
		m_light3Colour = ImVec4(m_LightManager->lights()[3].GetDiffuseColour().x, m_LightManager->lights()[3].GetDiffuseColour().y, m_LightManager->lights()[3].GetDiffuseColour().z, 1.f);

		// waves
		m_wavesHeight = m_ShaderManager->GetRippleShader()->GetWaveHeight();
		m_wavesFrequency = m_ShaderManager->GetRippleShader()->GetWaveFrequency();

		// terrain tesselation
		m_tesselationFactor = m_ShaderManager->GetTesselationShader()->GetTesselationFactor();
		m_tesselationEditsEnabled = m_ShaderManager->GetTesselationShader()->GetCameraBasedTesselationDisabled();
		
		// particles
		m_particlePositionVariance[0] = m_ParticleSystem->GetPositionVariance().x;
		m_particlePositionVariance[1] = m_ParticleSystem->GetPositionVariance().y;
		m_particlePositionVariance[2] = m_ParticleSystem->GetPositionVariance().z;

		m_particleMinimumVelocity = m_ParticleSystem->GetMinimumSpawnVelocity().y;

		m_particleMinimumLifespan = m_ParticleSystem->GetMinimumLifespan();

		// blur level
		m_blurLevel = m_ShaderManager->GetBlurLevel();

		BuildGUI();
		UpdateFromGUI();
	}

	void GUI::BuildGUI()
	{
		ImGui::Text("Shader Application");
		
		ImGui::Separator();


		ImGui::Text("VIEW MODE");
		ImGui::Checkbox("Wireframe on/off", &m_wireFrameEnabled);

		ImGui::Separator();

		ImGui::Text("BLUR");
		ImGui::SliderInt("Blur level (0 = off):", &m_blurLevel, 0, 3);
		if (m_blurLevel != 0) ImGui::Checkbox("Show Blur Stages?", &m_showPostProcessStages);

		ImGui::Separator();

		ImGui::Text("LIGHTING");
		ImGui::Checkbox("Light 1 on/off", &m_light1On);
		ImGui::ColorEdit3("Light 1 Colour", (float*)&m_light1Colour);
		ImGui::Checkbox("Light 2 on/off", &m_light2On);
		ImGui::ColorEdit3("Light 2 Colour", (float*)&m_light2Colour);
		ImGui::Checkbox("Light 3 on/off", &m_light3On);
		ImGui::ColorEdit3("Light 3 Colour", (float*)&m_light3Colour);
		ImGui::Checkbox("Show Shadow Depth Pass?", &m_showShadowDepthPass);

		ImGui::Separator();

		ImGui::Text("VERTEX MANIPULATION");
		ImGui::SliderFloat("Height of Waves", &m_wavesHeight, 0.1f, 1.f);
		ImGui::SliderFloat("Frequency of Waves", &m_wavesFrequency, 0.4f, 5.f);

		ImGui::Separator();

		ImGui::Text("TESSELATION");
		ImGui::Checkbox("Terrain Tesselation Editing on/off", &m_tesselationEditsEnabled);
		ImGui::SliderInt("Terrain Tesselation Factor", &m_tesselationFactor, 1, 64);

		ImGui::Separator();

		ImGui::Text("PARTICLES");
		ImGui::DragFloat3("Position Variance", m_particlePositionVariance, 1.f, 0.f, 200.f);
		ImGui::DragFloat("Minimum Velocity", &m_particleMinimumVelocity, 1.f, 100.f, 300.f);
		ImGui::DragFloat("Minimum Lifespan", &m_particleMinimumLifespan, 1.f, 50.f, 400.f);
	}

	void GUI::UpdateFromGUI()
	{
		// update lights : light manager
		m_LightManager->lights()[1].SetOn(m_light1On);
		m_LightManager->lights()[1].SetDiffuseColour(m_light1Colour.x, m_light1Colour.y, m_light1Colour.z, 1.f);

		m_LightManager->lights()[2].SetOn(m_light2On);
		m_LightManager->lights()[2].SetDiffuseColour(m_light2Colour.x, m_light2Colour.y, m_light2Colour.z, 1.f);
		m_LightManager->lights()[3].SetOn(m_light3On);
		m_LightManager->lights()[3].SetDiffuseColour(m_light3Colour.x, m_light3Colour.y, m_light3Colour.z, 1.f);

		// update waves : ripple shader
		m_ShaderManager->GetRippleShader()->SetWaveHeight(m_wavesHeight);
		m_ShaderManager->GetRippleShader()->SetWaveFrequency(m_wavesFrequency);

		// update terrain : tesselation shader
		m_ShaderManager->GetTesselationShader()->SetCameraBasedTesselationDisabled(m_tesselationEditsEnabled);
		m_ShaderManager->GetTesselationShader()->SetTesselationFactor(m_tesselationFactor);

		// update particles : particle system
		m_ParticleSystem->SetPositionVariance(XMFLOAT3(m_particlePositionVariance[0], m_particlePositionVariance[1], m_particlePositionVariance[2]));
		m_ParticleSystem->SetMinimumSpawnVelocity(XMFLOAT3(m_ParticleSystem->GetMinimumSpawnVelocity().x, m_particleMinimumVelocity, m_ParticleSystem->GetMinimumSpawnVelocity().z));
		m_ParticleSystem->SetMinimumLifeSpan(m_particleMinimumLifespan);

		// update blur : shader manager
		switch (m_blurLevel)
		{
		case BlurLevel::OFF:
			m_ShaderManager->SetBlurLevel(BlurLevel::OFF);
			break;
		case BlurLevel::LIGHT:
			m_ShaderManager->SetBlurLevel(BlurLevel::LIGHT);
			break;
		case BlurLevel::MEDIUM:
			m_ShaderManager->SetBlurLevel(BlurLevel::MEDIUM);
			break;
		case BlurLevel::HEAVY:
			m_ShaderManager->SetBlurLevel(BlurLevel::HEAVY);
			break;
		}
	}

	void GUI::Render(ID3D11DeviceContext* deviceContext)
	{
		// Force turn off on Geometry shader
		deviceContext->GSSetShader(NULL, NULL, 0);

		// Render UI
		 ImGui::Render();
	}

}