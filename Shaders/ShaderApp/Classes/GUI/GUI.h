#ifndef _GUI_H_
#define _GUI_H_

#include <D3D.h>
#include <d3d11.h>
#include <../DXFramework/imgui.h>
#include <../DXFramework/imgui_impl_dx11.h>
#include <Shaders/ShaderManager.h>
#include <Lights/LightManager.h>
#include <Scene/ParticleSystem.h>

// GUI is the container class for the IMGUI interface
// This allows IMGUI widgets to amend variables via the relevant getters and setters, rather than by direct-access.

namespace ShaderApp
{
	class GUI
	{
	public:
		GUI();
		~GUI();

		void Init(ShaderManager* shaderManager, LightManager* lightManager, ParticleSystem* particleSystem);
		void Update();
		void Render(ID3D11DeviceContext* deviceContext);

		inline bool GetWireFrameEnabled() { return m_wireFrameEnabled; }
		inline bool GetShowShadowDepthPass() { return m_showShadowDepthPass; }
		inline bool GetShowPostProcessStages() { return m_showPostProcessStages; }

	private:
		// Build ImGUI widgets
		void BuildGUI();
		// Update variables from buffer values held here
		void UpdateFromGUI();

		// pointer to the shader manager
		ShaderManager* m_ShaderManager;
		// pointer to the light manager
		LightManager* m_LightManager;
		// pointer to the particle system
		ParticleSystem* m_ParticleSystem;
		
		// true if wireframe is enabled
		bool m_wireFrameEnabled;

		// debug windows
		// switch on / off
		bool m_showShadowDepthPass;
		bool m_showPostProcessStages;

		// LIGHTS **********************************************
		// switch on / off
		bool m_light1On;
		bool m_light2On;
		bool m_light3On;

		// change colour
		ImVec4 m_light1Colour;
		ImVec4 m_light2Colour;
		ImVec4 m_light3Colour;

		// WAVES ***********************************************
		float m_wavesHeight;
		float m_wavesFrequency;

		// TERRAIN TESSELATION *********************************
		bool m_tesselationEditsEnabled;
		int m_tesselationFactor;

		// PARTICLES *******************************************
		// position variance
		float m_particlePositionVariance[3];
		// min speed
		float m_particleMinimumVelocity;
		// min lifespan
		float m_particleMinimumLifespan;

		// BLUR ************************************************
		int m_blurLevel;
	};
}

#endif
