#include "SceneObject.h"

namespace ShaderApp
{
	SceneObject::SceneObject() : m_mesh(nullptr), m_ShaderManager(nullptr), m_DeviceContext(nullptr)
	{
		m_rotationSpeed = 0.f;
	}

	SceneObject::~SceneObject()
	{
		CleanUp();
	}


	void SceneObject::Init(BaseMesh* mesh, ShaderManager* shaderManager, ID3D11DeviceContext* deviceContext, XMFLOAT4 pos, XMFLOAT4 rot, XMFLOAT4 scale)
	{
		m_mesh = mesh;
		m_ShaderManager = shaderManager;
		m_DeviceContext = deviceContext;

		m_position = pos;
		m_rotation = rot;
		m_scale = scale;
		
		UpdateTransform();
	}
	
	void SceneObject::Update(float dt)
	{
	}

	void SceneObject::Rotate(float dt, char axis)
	{
		XMFLOAT4 newRotation = GetRotation();
		float newComponent;

		// apply rotation dependent on supplied axis
		switch (axis)
		{
		case 'x':
			newComponent = newRotation.x + m_rotationSpeed * dt;
			newRotation.x = newComponent;
			break;

		case 'y':
			newComponent = newRotation.y + m_rotationSpeed * dt;
			newRotation.y = newComponent;
			break;

		case 'z':
			newComponent = newRotation.z + m_rotationSpeed * dt;
			newRotation.z = newComponent;
			break;
		}

		SetRotation(newRotation);
	}

	// Render using texture / depth shader	
	void SceneObject::Render(ShaderManager::ShaderType shaderType, RenderMatrixGroup& matrices, ID3D11ShaderResourceView* alt_texture)
	{
		// send mesh data
		GetMesh()->SendData(m_DeviceContext);

		// get transformed world matrix
		XMMATRIX tWorldMatrix;
		tWorldMatrix = matrices.worldMatrix * m_Transform.GetMatrix();

		// check shadertype
		switch (shaderType)
		{
		case ShaderManager::TEXTURE:
			// set shader parameters & render via shader manager:
			
			// check if an alternative texture has been supplied
			if (alt_texture) m_ShaderManager->SetShaderParameters(shaderType, tWorldMatrix, matrices.viewMatrix, matrices.projectionMatrix, alt_texture);
			else m_ShaderManager->SetShaderParameters(shaderType, tWorldMatrix, matrices.viewMatrix, matrices.projectionMatrix, GetMesh()->GetTexture());

			m_ShaderManager->Render(shaderType, GetMesh()->GetIndexCount());
		
			break;
	
		case ShaderManager::DEPTH:
			// set shader parameters & render via shader manager
			m_ShaderManager->SetShaderParameters(shaderType, tWorldMatrix, matrices.viewMatrix, matrices.projectionMatrix);
			m_ShaderManager->Render(shaderType, GetMesh()->GetIndexCount());
			break;

		default:
			break;
		}
	}

	// Render using light / ripple shader
	void SceneObject::Render(ShaderManager::ShaderType shaderType, RenderMatrixGroup& matrices, Light* lights, XMFLOAT3 cameraPos)
	{
		// send mesh data
		GetMesh()->SendData(m_DeviceContext);

		// get transformed world matrix
		const XMMATRIX tWorldMatrix = matrices.worldMatrix * m_Transform.GetMatrix();
	
		// validity check
		if ((shaderType == ShaderManager::LIGHT) || (shaderType == ShaderManager::RIPPLE))
		{
			// set shader parameters and render via shader manager
			m_ShaderManager->SetShaderParameters(shaderType, tWorldMatrix, matrices.viewMatrix, matrices.projectionMatrix, GetMesh()->GetTexture(), lights, cameraPos);
			m_ShaderManager->Render(shaderType, GetMesh()->GetIndexCount());
		}
	}

	// Render using shadow shader
	void SceneObject::Render(ShaderManager::ShaderType shaderType, RenderMatrixGroup& matrices, ID3D11ShaderResourceView* shadow_map, Light* light, Camera* camera)
	{
		// send mesh data
		GetMesh()->SendData(m_DeviceContext);

		// get transformed world matrix
		XMMATRIX tWorldMatrix;
		tWorldMatrix = matrices.worldMatrix * m_Transform.GetMatrix();

		// validity check
		if (shaderType == ShaderManager::SHADOW)
		{
			// set shader parameters and render via shader manager
			m_ShaderManager->SetShaderParameters(shaderType, tWorldMatrix, matrices.viewMatrix, matrices.projectionMatrix, GetMesh()->GetTexture(), shadow_map, light, camera);
			m_ShaderManager->Render(shaderType, GetMesh()->GetIndexCount());
		}
	}

	// Render using a blur shader
	void SceneObject::Render(ShaderManager::ShaderType shaderType, RenderMatrixGroup& matrices, ID3D11ShaderResourceView* texture, ID3D11ShaderResourceView* depthMap, float screen_dimension)
	{
		// send mesh data
		GetMesh()->SendData(m_DeviceContext);

		// get transformed world matrix
		XMMATRIX tWorldMatrix;
		tWorldMatrix = matrices.worldMatrix * m_Transform.GetMatrix();

		switch (shaderType)
		{
		case ShaderManager::BLUR_HORIZONTAL:
			m_ShaderManager->SetShaderParameters(shaderType, tWorldMatrix, matrices.viewMatrix, matrices.projectionMatrix, texture, depthMap, screen_dimension);
			m_ShaderManager->Render(shaderType, GetMesh()->GetIndexCount());
			break;
		case ShaderManager::BLUR_VERTICAL:
			m_ShaderManager->SetShaderParameters(shaderType, tWorldMatrix, matrices.viewMatrix, matrices.projectionMatrix, texture, depthMap, screen_dimension);
			m_ShaderManager->Render(shaderType, GetMesh()->GetIndexCount());
			break;
		default:
			break;
		}
	}

	void SceneObject::CleanUp()
	{
	}

	void SceneObject::SetPosition(XMFLOAT4 new_pos)
	{
		// update posiiton and set translation
		m_position = new_pos;
		m_Transform.SetTranslation(m_position);
	}

	void SceneObject::SetRotation(XMFLOAT4 new_rot)
	{
		// update rotation and transform
		m_rotation = new_rot;
		UpdateTransform();
	}

	void SceneObject::SetScale(XMFLOAT4 new_scale)
	{
		// update scale and transform
		m_scale = new_scale;
		UpdateTransform();
	}
	
	void SceneObject::UpdateTransform()
	{
		// Set transform to identity matrix
		m_Transform.SetIdentity();

		// Update translation
		m_Transform.SetTranslation(m_position);

		// Update rotation
		// Get Rotation matrices
		Transform rotation_x;
		rotation_x.RotationX(m_rotation.x);
		Transform rotation_y;
		rotation_y.RotationY(m_rotation.y);
		Transform rotation_z;
		rotation_z.RotationZ(m_rotation.z);
		// Multiply with transform matrix to get translation + rotation 
		XMMATRIX rotation_matrix = rotation_x.GetMatrix() * rotation_y.GetMatrix() * rotation_z.GetMatrix() * m_Transform.GetMatrix();
		m_Transform.SetMatrix(rotation_matrix);
		
		// Update Scale
		Transform scale;
		scale.Scale(m_scale);
		XMMATRIX scale_matrix = scale.GetMatrix() * m_Transform.GetMatrix();
		m_Transform.SetMatrix(scale_matrix);

	}

	// return position
	XMFLOAT3 SceneObject::GetPositionXMF3() const
	{
		XMFLOAT3 result;
		result = XMFLOAT3(m_position.x, m_position.y, m_position.z);
		return result;
	}

}