#ifndef _PARTICLESYSTEM_H_
#define _PARTICLESYSTEM_H_

#include "SceneObject.h"
#include "ParticleData.h"

#include <../DXFramework/ParticleSystemMesh.h>
#include <Utilities/RNG.h>
#include <Utilities/MathsUtilities.h>

// Particle System conatins the particle data array that defines the system
// It's reponsible for updating the particles, spawning and destroying them

namespace ShaderApp
{
	class ParticleSystem : public SceneObject
	{
	public:
		ParticleSystem();
		~ParticleSystem();

		void Init(ParticleSystemMesh* particleMesh, ShaderManager* shaderManager, ID3D11DeviceContext* deviceContext,
			XMFLOAT4 pos = XMFLOAT4(0.f, 0.f, 0.f, 0.f), XMFLOAT4 rot = XMFLOAT4(0.f, 0.f, 0.f, 0.f), XMFLOAT4 scale = XMFLOAT4(0.f, 0.f, 0.f, 0.f));

		void Update(float dt, XMFLOAT3 cameraPos);
		void Render(ShaderManager::ShaderType shaderType, RenderMatrixGroup& matrices, Light* light);
		void Render(ShaderManager::ShaderType shaderType, RenderMatrixGroup& matrices);

		// getters and setters

		// return particle mesh
		inline const class ParticleSystemMesh* GetParticleMesh() const { return m_particleMesh; }
		// the mesh that visually represents this scene object
		inline void SetParticleMesh(const ParticleSystemMesh* particle_mesh) { m_particleMesh = particle_mesh; }
		// return pointer to index 0 in particle data array
		inline ParticleData* GetParticleData() { return m_ParticleData; }
		// return arc cosine angle (for debug)
		inline float GetArcCosAngle() { return m_arcCosAngle; }

		// get / set position variance
		inline XMFLOAT3 GetPositionVariance() { return m_positionVariance; }
		inline void SetPositionVariance(XMFLOAT3 newPosVar) { m_positionVariance = newPosVar; }
		
		// get / set minimum spawn velocity
		inline XMFLOAT3 GetMinimumSpawnVelocity() { return m_minimumSpawnVelocity; }
		inline void SetMinimumSpawnVelocity(XMFLOAT3 newMinVel) {  m_minimumSpawnVelocity = newMinVel; }
		
		// get / set minimum lifespan
		inline float GetMinimumLifespan() { return m_minimumLifespan; }
		inline void SetMinimumLifeSpan(float newMinLifespan) { m_minimumLifespan = newMinLifespan; }

	private:
		// Initialise particle data
		void InitParticleData();
		// Populate and return a new spawn data
		ParticleData::SpawnData GetNewSpawnData();
		// Spawn a particle
		void SpawnParticle(int particle_index);
		// Rotate to face the camera
		void FaceCamera(XMFLOAT3 cameraPosition);

		// The mesh containing the points used to generate the particles
		const ParticleSystemMesh* m_particleMesh;
		// Array holding data correspinding to each particle
		ParticleData m_ParticleData[P_SYSTEMSIZE];

		// Random number generator
		RNG m_RNG;

		// Particle variance allowances
		XMFLOAT3 m_positionVariance;
		XMFLOAT3 m_velocityVariance;
		XMFLOAT3 m_minimumSpawnVelocity;

		float m_lifespanVariance;
		float m_minimumLifespan;

		float m_arcCosAngle;

	};
}

#endif // !_PARTICLESYSTEM_H_
