#include "ParticleSystem.h"

namespace ShaderApp
{
	ParticleSystem::ParticleSystem() : m_particleMesh(nullptr)
	{
		m_positionVariance = XMFLOAT3(100.f, 100.f, 50.f);
		m_velocityVariance = XMFLOAT3(0.f, 100.f, 0.f);
		m_lifespanVariance = 100.f;

		m_minimumSpawnVelocity = XMFLOAT3(0.f, 50.f, 0.f);
		m_minimumLifespan = 100.f;
	}

	ParticleSystem::~ParticleSystem()
	{
		if (m_particleMesh)
		{
			delete m_particleMesh;
			m_particleMesh = nullptr;
		}
	}


	void ParticleSystem::Init(ParticleSystemMesh* particleMesh, ShaderManager* shaderManager, ID3D11DeviceContext* deviceContext,
		XMFLOAT4 pos, XMFLOAT4 rot, XMFLOAT4 scale)
	{
		m_particleMesh = particleMesh;
		SceneObject::Init(nullptr, shaderManager, deviceContext, pos, rot, scale);

		InitParticleData();
	}


	void ParticleSystem::InitParticleData()
	{
		for (int i = 0; i < P_SYSTEMSIZE; ++i)
		{
			SpawnParticle(i);
		}
	}

	void ParticleSystem::Update(float dt, XMFLOAT3 cameraPos)
	{
		for (int i = 0; i < P_SYSTEMSIZE; ++i)
		{
			// update particle data
			m_ParticleData[i].Update(dt);

			// check for dead particles
			if (!m_ParticleData[i].IsLive()) SpawnParticle(i);
		}

		FaceCamera(cameraPos);
	}

	ParticleData::SpawnData ParticleSystem::GetNewSpawnData()
	{
		ParticleData::SpawnData result;

		// set position
		XMFLOAT3 pos = XMFLOAT3(0.f, 0.f, 0.f);
		if (m_positionVariance.x != 0.f) pos.x = m_RNG.RandomRange(-m_positionVariance.x, m_positionVariance.x) / 100.f;
		if (m_positionVariance.y != 0.f) pos.y = m_RNG.RandomRange(-m_positionVariance.y, m_positionVariance.y) / 100.f;
		if (m_positionVariance.z != 0.f) pos.z = m_RNG.RandomRange(-m_positionVariance.z, m_positionVariance.z) / 100.f;

		// These positions are provided direct to the Geometry Shader, so matrix transforms will not affect the actual particles' position
		pos.x += m_position.x;
		pos.y += m_position.y;
		pos.z += m_position.z;

		result.position = pos;

		// set velocity
		XMFLOAT3 vel = XMFLOAT3(0.f, 0.f, 0.f);
		if (m_velocityVariance.x != 0.f) vel.x = m_RNG.RandomRange(m_minimumSpawnVelocity.x, (m_minimumSpawnVelocity.x + m_velocityVariance.x)) / 100.f;
		if (m_velocityVariance.y != 0.f) vel.y = m_RNG.RandomRange(m_minimumSpawnVelocity.y, (m_minimumSpawnVelocity.y + m_velocityVariance.y)) / 100.f;
		if (m_velocityVariance.z != 0.f) vel.z = m_RNG.RandomRange(m_minimumSpawnVelocity.z, (m_minimumSpawnVelocity.z + m_velocityVariance.z)) / 100.f;
		result.velocity = vel;

		// set lifespan
		float ls = 0.f;
		if (m_lifespanVariance != 0.f) ls = m_RNG.RandomRange(m_minimumLifespan, (m_minimumLifespan + m_lifespanVariance)) / 100.f;
		result.lifespan = ls;

		return result;
	}

	void ParticleSystem::SpawnParticle(int particle_index)
	{
		ParticleData::SpawnData spawnData = GetNewSpawnData();
		m_ParticleData[particle_index].Spawn(spawnData);
	}

	void ParticleSystem::FaceCamera(XMFLOAT3 cameraPosition)
	{
		// find vector between particle system and camera
		XMFLOAT3 v1;
		v1.x = cameraPosition.x - GetPosition().x;
		v1.y = cameraPosition.y - GetPosition().y;
		v1.z = cameraPosition.z - GetPosition().z;

		// 0 degrees rotation around the y axis requires facing -1 on the z axis
		XMFLOAT3 v2 = XMFLOAT3(0.f, 0.f, -1.f);

		// get dot product of v1 & v2.
		float dot = v1.x * v2.x + v1.z * v2.z;

		// get length of each vector
		float lengthV1sq = v1.x * v1.x + v1.z * v1.z;
		float lengthV2sq = v2.x * v2.x + v2.z * v2.z;
		float lengthV1 = sqrtf(lengthV1sq);
		float lengthV2 = sqrtf(lengthV2sq);
		
		// find the cosine of the angle between the vectors
		m_arcCosAngle = dot / (lengthV1 * lengthV2);

		// get inverse cosine of this value.
		float angle = acosf(m_arcCosAngle);

		// apply quadrant correction to give 360 rotation
		if (cameraPosition.x > GetPosition().x) angle = (2.f * (float)PI) - angle;

		// create a new rotation about the y axis.
		XMFLOAT4 newRotation = GetRotation();
		newRotation.y = angle;

		// set new rotations
		SetRotation(newRotation);
	}


	void ParticleSystem::Render(ShaderManager::ShaderType shaderType, RenderMatrixGroup& matrices, Light* light)
	{
		if (shaderType == ShaderManager::PARTICLE)
		{
			// send mesh data 
			m_particleMesh->SendData(m_DeviceContext);

			// get transformed world matrix
			XMMATRIX tWorldMatrix;
			tWorldMatrix = matrices.worldMatrix * m_Transform.GetMatrix();
			// set shader parameters
			m_ShaderManager->SetShaderParameters(shaderType, tWorldMatrix, matrices.viewMatrix, matrices.projectionMatrix,
				m_particleMesh->GetTexture(), this, light);
			// render via shader manager
			m_ShaderManager->Render(shaderType, m_particleMesh->GetIndexCount());
		}
	}

	void ParticleSystem::Render(ShaderManager::ShaderType shaderType, RenderMatrixGroup& matrices)
	{
		if (shaderType == ShaderManager::PARTICLE_DEPTH)
		{
			// send mesh data 
			m_particleMesh->SendData(m_DeviceContext);

			// get transformed world matrix
			XMMATRIX tWorldMatrix;
			tWorldMatrix = matrices.worldMatrix * m_Transform.GetMatrix();
			// set shader parameters
			m_ShaderManager->SetShaderParameters(shaderType, tWorldMatrix, matrices.viewMatrix, matrices.projectionMatrix, this);
			// render via shader manager
			m_ShaderManager->Render(shaderType, m_particleMesh->GetIndexCount());
		}
		
	}
	

}
