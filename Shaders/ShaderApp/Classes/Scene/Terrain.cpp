#include "Terrain.h"

namespace ShaderApp
{
	Terrain::Terrain() : 
		m_heightMap(nullptr),
		m_tesselationMesh(nullptr)
	{
		SceneObject::SceneObject();
	}

	Terrain::~Terrain()
	{
	}

	void Terrain::Init(TessellationQuadMesh* mesh, ShaderManager* shaderManager, ID3D11Device* device, ID3D11DeviceContext* deviceContext, WCHAR* heightMapFilename,
		XMFLOAT4 pos, XMFLOAT4 rot, XMFLOAT4 scale)
	{
		// set tesselation mesh
		m_tesselationMesh = mesh;
		// load in the height map
		LoadHeightMap(device, deviceContext, heightMapFilename);
		// call base init
		SceneObject::Init(mesh, shaderManager, deviceContext, pos, rot, scale);
	}

	void Terrain::LoadHeightMap(ID3D11Device* device, ID3D11DeviceContext* deviceContext, WCHAR* fileName)
	{
		// Create the heightmap texture object.
		m_heightMap = new Texture(device, deviceContext, fileName);
	}

	void Terrain::Render(ShaderManager::ShaderType shaderType, RenderMatrixGroup& matrices, Light* light)
	{
		// send mesh data
		m_tesselationMesh->SendData(m_DeviceContext);

		// get transformed world matrix
		XMMATRIX tWorldMatrix;
		tWorldMatrix = matrices.worldMatrix * m_Transform.GetMatrix();

		// validity check
		if (shaderType == ShaderManager::TESSELATION)
		{
			// set shader parameters and render via shader manager
			m_ShaderManager->SetShaderParameters(shaderType, tWorldMatrix, matrices.viewMatrix, matrices.projectionMatrix, light, GetMesh()->GetTexture(), m_heightMap->GetTexture());
			m_ShaderManager->Render(shaderType, GetMesh()->GetIndexCount());
		}

	}

	void Terrain::Render(ShaderManager::ShaderType shaderType, RenderMatrixGroup& matrices)
	{
		// send mesh data
		m_tesselationMesh->SendData(m_DeviceContext);

		// get transformed world matrix
		XMMATRIX tWorldMatrix;
		tWorldMatrix = matrices.worldMatrix * m_Transform.GetMatrix();

		// validity check
		if (shaderType == ShaderManager::TESSELATION_DEPTH)
		{
			// set shader parameters and render via shader manager
			m_ShaderManager->SetShaderParameters(shaderType, tWorldMatrix, matrices.viewMatrix, matrices.projectionMatrix, m_heightMap->GetTexture());
			m_ShaderManager->Render(shaderType, GetMesh()->GetIndexCount());
		}
	}

}