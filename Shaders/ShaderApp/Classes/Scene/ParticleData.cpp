#include "ParticleData.h"

namespace ShaderApp
{
	ParticleData::ParticleData()
	{
	}

	ParticleData::~ParticleData()
	{
	}

	void ParticleData::Spawn(SpawnData& spawnData)
	{
		m_position = spawnData.position;
		m_velocity = spawnData.velocity;
		m_lifeSpan = spawnData.lifespan;
		m_timer = 0.f;
	}

	void ParticleData::Update(float dt)
	{
		// increment timer
		m_timer += dt;

		// move
		m_position.x += (m_velocity.x * dt);
		m_position.y += (m_velocity.y * dt);
		m_position.z += (m_velocity.z * dt);
	}

	bool ParticleData::IsLive()
	{
		if (m_timer >= m_lifeSpan) return false;
		else return true;
	}

	XMFLOAT4 ParticleData::GetPosition()
	{
		XMFLOAT4 result = XMFLOAT4(m_position.x, m_position.y, m_position.z, 1.f);
		return result;
	}

	float ParticleData::GetScale()
	{
		// get scale as inverse of time / lifespan
		float result = 1 - (m_timer / m_lifeSpan);
		// clamp result
		if (result > 1) result = 1.f;
		else if (result <= 0) result = 0.1f;

		return result;

	}
}
