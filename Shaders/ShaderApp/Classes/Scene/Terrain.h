#ifndef _TERRAIN_H_
#define _TERRAIN_H_

#include "SceneObject.h"
#include <../DXFramework/TessellationQuadMesh.h>

// Terrain is a Scene Object with a height map to allow for displacement of vertices during tesselation

namespace ShaderApp
{
	class Terrain : public SceneObject
	{
	public:
		Terrain();
		~Terrain();

		void Init(TessellationQuadMesh* mesh, ShaderManager* shaderManager, ID3D11Device* device, ID3D11DeviceContext* deviceContext, WCHAR* heightMapFilename,
			XMFLOAT4 pos = XMFLOAT4(0.f, 0.f, 0.f, 0.f), XMFLOAT4 rot = XMFLOAT4(0.f, 0.f, 0.f, 0.f), XMFLOAT4 scale = XMFLOAT4(1.f, 1.f, 1.f, 0.f));
		void Render(ShaderManager::ShaderType shaderType, RenderMatrixGroup& matrices, Light* light);
		void Render(ShaderManager::ShaderType shaderType, RenderMatrixGroup& matrices);

	private:
		void LoadHeightMap(ID3D11Device* device, ID3D11DeviceContext* deviceContext, WCHAR* fileName);

		// The terrain's heightmap
		Texture* m_heightMap;

		// The mesh used for tesselation
		const TessellationQuadMesh* m_tesselationMesh;
	};
}

#endif