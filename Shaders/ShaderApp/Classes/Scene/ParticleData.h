#ifndef _PARTICLEDATA_H_
#define _PARTICLEDATA_H_

#include <../DXFramework/D3D.h>

// Contains the data that defines a particle within the particle system

namespace ShaderApp
{
	class ParticleData
	{
	public:
		// Data struct used to spawn particle
		struct SpawnData
		{
			XMFLOAT3 position, velocity;
			float lifespan;
		};

		ParticleData();
		~ParticleData();

		// Spawn this particle
		void Spawn(SpawnData& spawnData);
		void Update(float dt);

		// Return true is particle is 'alive'
		bool IsLive();

		// Getters
		XMFLOAT4 GetPosition();
		float GetScale();

	private:
		// This particle's position
		XMFLOAT3 m_position;
		// This particle's velocity
		XMFLOAT3 m_velocity;
		// This particle's lifespan
		float m_lifeSpan;
		// This particle's lifespan timer
		float m_timer;
	};
}

#endif
