#ifndef _SCENEOBJECT_H_
#define _SCENEOBJECT_H_

#include <../DXFramework/BaseMesh.h>
#include <Maths/Transform.h>
#include <Shaders/ShaderManager.h>

// The Scene Object defines a basic objects wiothin the scene, and allows a mesh to be transformed and rendered more easily

namespace ShaderApp
{
	class SceneObject
	{
	public:
		SceneObject();
		virtual~SceneObject();

		// Init
		void Init(BaseMesh* mesh, ShaderManager* shaderManager, ID3D11DeviceContext* deviceContext,
			XMFLOAT4 pos = XMFLOAT4(0.f, 0.f, 0.f, 0.f), XMFLOAT4 rot = XMFLOAT4(0.f, 0.f, 0.f, 0.f), XMFLOAT4 scale = XMFLOAT4(1.f, 1.f, 1.f, 0.f));

		// Update
		virtual void Update(float dt);
		// Rotate object by its rotate speed about specified axis
		void Rotate(float dt, char axis);
		// Render using light / ripple shader
		void Render(ShaderManager::ShaderType shaderType, RenderMatrixGroup& matrices, Light* lights, XMFLOAT3 cameraPos);
		// Render using texture / depth
		// if alt texture is valid, mesh will be rendered using the texture shader, with the supplied texture instead of the mesh's own texture
		void Render(ShaderManager::ShaderType shaderType, RenderMatrixGroup& matrices, ID3D11ShaderResourceView* alt_texture = nullptr);
		// Render using shadow shader
		void Render(ShaderManager::ShaderType shaderType, RenderMatrixGroup& matrices, ID3D11ShaderResourceView* shadow_map, Light* light, Camera* camera);
		// Render using a blur shader
		void Render(ShaderManager::ShaderType shaderType, RenderMatrixGroup& matrices, ID3D11ShaderResourceView* texture, ID3D11ShaderResourceView* depthMap, float screen_dimension);

		// CleanUp
		void CleanUp();

		// return the transformation matrix
		inline const Transform& GetTransform() const { return m_Transform; }
		// set the transformation matrix to param 'transform'
		inline void SetTransform(const Transform& transform) { m_Transform = transform; }

		// return mesh
		// Note: This prohibits editing the mesh, so that several scene objects can use one mesh
		inline const class BaseMesh* GetMesh() const { return m_mesh; }
		// the mesh that visually represents this scene object
		inline void SetMesh(const BaseMesh* mesh) { m_mesh = mesh; }

		// return position
		inline XMFLOAT4 GetPosition() const { return m_position; }
		// return position
		XMFLOAT3 GetPositionXMF3() const;
		// set position to new_pos
		void SetPosition(XMFLOAT4 new_pos);

		// return rotation
		inline XMFLOAT4 GetRotation() const { return m_rotation; }
		// set rotation to new_rot
		void SetRotation(XMFLOAT4 new_rot);

		inline float GetRotationSpeed() const { return m_rotationSpeed; }
		void SetRotationSpeed(float newSpeed) { m_rotationSpeed = newSpeed; }

		// return scale
		inline XMFLOAT4 GetScale() const { return m_scale; }
		// set scale to new_scale
		void SetScale(XMFLOAT4 new_scale);

	protected:
		// Pointer to the shader manager, to allow object to draw itself
		ShaderManager* m_ShaderManager;
		// Pointer o the device context, again to assist with rendering
		ID3D11DeviceContext* m_DeviceContext;

		// Recalculate scene object's transform matrix
		void UpdateTransform();
		// The scene object's mesh
		const BaseMesh* m_mesh;
		// The scene object's transform
		Transform m_Transform;

		XMFLOAT4 m_position;		// Scene Object Position in World Space
		XMFLOAT4 m_rotation;		// Scene Object Rotation about x, y, z, axies
		XMFLOAT4 m_scale;			// Scene Object Scale

		float m_rotationSpeed;		// Scene Object's rotation speed
	};
}

#endif // !_SCENEOBJECT_H_
