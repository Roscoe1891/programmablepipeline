#include "Transform.h"

namespace ShaderApp
{
	Transform::Transform()
	{
	}

	Transform::~Transform()
	{
	}

	void Transform::SetIdentity()
	{
		m_matrix = XMMATRIX(1.0f, 0.0f, 0.0f, 0.0f,
			0.0f, 1.0f, 0.0f, 0.0f,
			0.0f, 0.0f, 1.0f, 0.0f,
			0.0f, 0.0f, 0.0f, 1.0f);
	}

	void Transform::SetZero()
	{
		m_matrix = XMMATRIX(0.0f, 0.0f, 0.0f, 0.0f,
			0.0f, 0.0f, 0.0f, 0.0f,
			0.0f, 0.0f, 0.0f, 0.0f,
			0.0f, 0.0f, 0.0f, 0.0f);
	}

	void Transform::RotationX(float radiansRot)
	{
		m_matrix = XMMATRIX(1.0f, 0.0f, 0.0f, 0.0f,
			0.0f, cosf(radiansRot), sinf(radiansRot), 0.0f,
			0.0f, -sinf(radiansRot), cosf(radiansRot), 0.0f,
			0.0f, 0.0f, 0.0f, 1.0f);
	}

	void Transform::RotationY(float radiansRot)
	{
		m_matrix = XMMATRIX(cosf(radiansRot), 0.0f, -sinf(radiansRot), 0.0f,
			0.0f, 1.0f, 0.0f, 0.0f,
			sinf(radiansRot), 0.0f, cosf(radiansRot), 0.0f,
			0.0f, 0.0f, 0.0f, 1.0f);	
	}

	void Transform::RotationZ(float radiansRot)
	{
		m_matrix = XMMATRIX(cosf(radiansRot), sinf(radiansRot), 0.0f, 0.0f,
			-sinf(radiansRot), cosf(radiansRot), 0.0f, 0.0f,
			0.0f, 0.0f, 1.0f, 0.0f,
			0.0f, 0.0f, 0.0f, 1.0f);
	}

	void Transform::Scale(const XMFLOAT4& scale)
	{
		SetIdentity();
		m_matrix.r[0] = XMVectorSetX(m_matrix.r[0], scale.x);
		m_matrix.r[1] = XMVectorSetY(m_matrix.r[1], scale.y);
		m_matrix.r[2] = XMVectorSetZ(m_matrix.r[2], scale.z);
		
	}

	XMFLOAT4 Transform::GetScale() const
	{

		float scale_x_length_sq = XMVectorGetX(m_matrix.r[0]) * XMVectorGetX(m_matrix.r[0]) +
			XMVectorGetY(m_matrix.r[0]) * XMVectorGetY(m_matrix.r[0]) +
			XMVectorGetZ(m_matrix.r[0]) * XMVectorGetZ(m_matrix.r[0]);

		float scale_x_length = sqrtf(scale_x_length_sq);

		float scale_y_length_sq = XMVectorGetX(m_matrix.r[1]) * XMVectorGetX(m_matrix.r[1]) +
			XMVectorGetY(m_matrix.r[1]) * XMVectorGetY(m_matrix.r[1]) +
			XMVectorGetZ(m_matrix.r[1]) * XMVectorGetZ(m_matrix.r[1]);

		float scale_y_length = sqrtf(scale_y_length_sq);

		float scale_z_length_sq = XMVectorGetX(m_matrix.r[2]) * XMVectorGetX(m_matrix.r[2]) +
			XMVectorGetY(m_matrix.r[2]) * XMVectorGetY(m_matrix.r[2]) +
			XMVectorGetZ(m_matrix.r[2]) * XMVectorGetZ(m_matrix.r[2]);

		float scale_z_length = sqrtf(scale_z_length_sq);

		return XMFLOAT4(scale_x_length, scale_y_length, scale_z_length, 0.f);
	}

	void Transform::SetTranslation(const XMFLOAT4& trans)
	{
		m_matrix.r[3] = XMVectorSetX(m_matrix.r[3], trans.x);
		m_matrix.r[3] = XMVectorSetY(m_matrix.r[3], trans.y);
		m_matrix.r[3] = XMVectorSetZ(m_matrix.r[3], trans.z);
	}

	const XMFLOAT4 Transform::GetTranslation() const
	{
		return XMFLOAT4(XMVectorGetX(m_matrix.r[3]), XMVectorGetY(m_matrix.r[3]), XMVectorGetZ(m_matrix.r[3]), 0.f);
	}

	const XMFLOAT3 Transform::GetTranslationF3() const
	{
		return XMFLOAT3(XMVectorGetX(m_matrix.r[3]), XMVectorGetY(m_matrix.r[3]), XMVectorGetZ(m_matrix.r[3]));
	}

}