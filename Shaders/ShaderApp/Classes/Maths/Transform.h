#ifndef _TRANSFORM_H_
#define _TRANSFORM_H_

#include <DirectXMath.h>

namespace ShaderApp
{
	using namespace DirectX;

	/// Transform contains an XMMatrix along with several utility functions to assist in creating a transform matrix
	/// This implementation was adapted from the Matrix44 class in the Games Education Framework by Grant Clarke
	
	class Transform
	{
	public:
		Transform();
		~Transform();

		/// @brief return m_matrix
		inline const XMMATRIX& GetMatrix() const { return m_matrix; }

		/// @brief set the transformation matrix
		/// @param in transform the new transform matrix
		inline void SetMatrix(const XMMATRIX& transform) { m_matrix = transform; }

		/// @brief Set m_matrix to the identity matrix
		void SetIdentity();

		/// @brief Set all elements of m_matrix to zero
		void SetZero();

		/// @brief Set m_matrix to a rotation matrix around the x-axis.
		/// @param[in] radiansRot	Angle of rotation.
		/// @note A positive angle of rotation results in a anti-clockwise rotation when looking towards the origin from the positive axis direction.
		/// @note This function overwrites all current elements of the matrix.
		void RotationX(float radiansRot);

		/// @brief Set m_matrix to a rotation matrix around the y-axis.
		/// @param[in] radiansRot	Angle of rotation.
		/// @note A positive angle of rotation results in a anti-clockwise rotation when looking towards the origin from the positive axis direction.
		/// @note This function overwrites all current elements of the matrix.
		void RotationY(float radiansRot);

		/// @brief Set m_matrix to a rotation matrix around the z-axis.
		/// @param[in] radiansRot	Angle of rotation.
		/// @note A positive angle of rotation results in a anti-clockwise rotation when looking towards the origin from the positive axis direction.
		/// @note This function overwrites all current elements of the matrix.
		void RotationZ(float radiansRot);

		/// @brief Set m_matrix to a scale matrix.
		/// @param[in] scale	Scale values for xyz axes.
		/// @note This function overwrites all current elements of the matrix.
		void Scale(const XMFLOAT4& scale);

		/// @brief Get the Scale values for the xyz axes.
		/// @return The scale values for the xyz axes.
		XMFLOAT4 GetScale() const;

		/// @brief Set the elements in m_matrix that represent the translation.
		/// @param[in] trans	The translation.
		/// @note This function only overwrites the elements where the translation is stored. All other elements remain unaltered.
		void SetTranslation(const XMFLOAT4& trans);

		/// @brief Get the translation from m_matrix.
		/// @return The translation.
		const XMFLOAT4 GetTranslation() const;

		/// @brief Get the translation from m_matrix.
		/// @return The translation as a XMFLOAT3.
		const XMFLOAT3 GetTranslationF3() const;


	private:
		XMMATRIX m_matrix;
	};
}

#endif