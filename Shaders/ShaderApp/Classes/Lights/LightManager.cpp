#include "LightManager.h"

namespace ShaderApp
{
	LightManager::LightManager()
	{
	}

	LightManager::~LightManager()
	{
	}

	void LightManager::InitLights()
	{
		// LIGHT 0 (Shadowcaster)
		m_Lights[0].SetAmbientColour(0.15f, 0.1f, 0.05f, 1.f);
		m_Lights[0].SetDiffuseColour(0.9f, 0.8f, 0.6f, 1.f);
		m_Lights[0].SetPosition(40.f, 12.f, -3.f);
		m_Lights[0].SetLookAt(0.f, 0.f, 0.f);
		m_Lights[0].SetSpecularPower(25.f);
		m_Lights[0].SetSpecularColour(0.2f, 0.2f, 0.2f, 1.f);
		m_Lights[0].SetAttenuationFactors(1.f, 0.f, 0.f);
		m_Lights[0].SetRange(140.f);
		m_Lights[0].SetOn(true);

		// Set up blue light
		m_Lights[1].SetAmbientColour(0.f, 0.f, 0.f, 1.f);
		m_Lights[1].SetDiffuseColour(0.f, 0.f, 1.f, 1.f);
		m_Lights[1].SetPosition(28.f, 1.f, 2.f);
		m_Lights[1].SetSpecularPower(25.f);
		m_Lights[1].SetSpecularColour(0.1f, 0.1f, 0.1f, 1.f);
		m_Lights[1].SetAttenuationFactors(0.f, 1.f, 0.f);
		m_Lights[1].SetRange(5.f);
		m_Lights[1].SetOn(true);

		// Set up magenta light
		m_Lights[2].SetAmbientColour(0.f, 0.f, 0.f, 1.f);
		m_Lights[2].SetDiffuseColour(0.5f, 0.f, 0.5f, 1.f);
		m_Lights[2].SetPosition(28.f, 1.f, -2.f);
		m_Lights[2].SetSpecularPower(25.f);
		m_Lights[2].SetSpecularColour(0.1f, 0.1f, 0.1f, 1.f);
		m_Lights[2].SetAttenuationFactors(0.f, 1.f, 0.f);
		m_Lights[2].SetRange(5.f);
		m_Lights[2].SetOn(true);

		// Set up cyan light
		m_Lights[3].SetAmbientColour(0.f, 0.f, 0.f, 1.f);
		m_Lights[3].SetDiffuseColour(0.f, 0.6f, 0.8f, 1.f);
		m_Lights[3].SetPosition(28.f, 1.f, 0.f);
		m_Lights[3].SetSpecularPower(25.f);
		m_Lights[3].SetSpecularColour(0.1f, 0.1f, 0.1f, 1.f);
		m_Lights[3].SetAttenuationFactors(0.f, 1.f, 0.f);
		m_Lights[3].SetRange(15.f);
		m_Lights[3].SetOn(true);
	}
}