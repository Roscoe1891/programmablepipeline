#ifndef _LIGHTMANAGER_H_
#define _LIGHTMANAGER_H_

#include "../DXFramework/Light.h"

// Light Manager contains 4 lights (the maximum we can have 'for free')
// Light 0 is treated by the shaders as the only light that casts shadows
// Other lights will not cast shadows, but will illuminate them

namespace ShaderApp
{

#define MAX_LIGHTS 4

	class LightManager
	{
	public:
		LightManager();
		~LightManager();

		void InitLights();

		// return pointer to light 0
		inline Light* lights() { return m_Lights; }

	private:
		// The lights array
		Light m_Lights[MAX_LIGHTS];
	};
}




#endif