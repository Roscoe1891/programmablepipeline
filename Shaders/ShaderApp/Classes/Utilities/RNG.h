#ifndef _RNG_H_
#define _RNG_H_

#include <ctime>
#include <cstdlib>

// Random number generator

namespace ShaderApp
{
	class RNG
	{
	public:
		RNG();
		~RNG();

		// return a random number
		int Random();				
		// return a random number within provided range (inclusive)
		int RandomRange(int min, int max);		
		// return a random number within provided range (inclusive)
		// allows input of floats however they will be treated as ints
		int RandomRange(float min, float max);

	private:

	};
}
#endif // !_RNG_H
