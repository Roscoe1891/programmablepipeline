#include "ParticleDepthShader.h"

namespace ShaderApp
{
	ParticleDepthShader::ParticleDepthShader(ID3D11Device* device, HWND hwnd) : BaseShader(device, hwnd)
	{
		m_matrixBuffer = nullptr;
		m_particlePositionBuffer = nullptr;
		m_particleScaleBuffer = nullptr;

		InitShader(L"ShaderFiles/particle_vs.hlsl", L"ShaderFiles/particleDepth_gs.hlsl", L"ShaderFiles/depth_ps.hlsl");
	}

	ParticleDepthShader::~ParticleDepthShader()
	{
		// Release the matrix constant buffer.
		if (m_matrixBuffer)
		{
			m_matrixBuffer->Release();
			m_matrixBuffer = 0;
		}

		// Release the layout
		if (m_layout)
		{
			m_layout->Release();
			m_layout = 0;
		}

		// Release the particle position constant buffer.
		if (m_particlePositionBuffer)
		{
			m_particlePositionBuffer->Release();
			m_particlePositionBuffer = 0;
		}

		// Release the particle scale constant buffer
		if (m_particleScaleBuffer)
		{
			m_particleScaleBuffer->Release();
			m_particleScaleBuffer = 0;
		}

		//Release base shader components
		BaseShader::~BaseShader();
	}

	void ParticleDepthShader::InitShader(WCHAR* vsFilename, WCHAR* psFilename)
	{
		D3D11_BUFFER_DESC matrixBufferDesc;
		D3D11_BUFFER_DESC particlePositionBufferDesc;
		D3D11_BUFFER_DESC particleScaleBufferDesc;

		// Load (+ compile) shader files
		loadVertexShader(vsFilename);
		loadPixelShader(psFilename);

		// *************************************** SET UP MATRIX BUFFER ***************************************  

		// Setup the description of the dynamic matrix constant buffer that is in the geometry shader.
		matrixBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
		matrixBufferDesc.ByteWidth = sizeof(MatrixBufferType);
		matrixBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		matrixBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		matrixBufferDesc.MiscFlags = 0;
		matrixBufferDesc.StructureByteStride = 0;

		// Create the constant buffer pointer so we can access the vertex shader constant buffer from within this class.
		m_device->CreateBuffer(&matrixBufferDesc, NULL, &m_matrixBuffer);

		// *************************************** SET UP PARTICLE POSITION BUFFER ***************************************  
		// Setup the description of the particle position constant buffer that is in the vertex shader.
		particlePositionBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
		particlePositionBufferDesc.ByteWidth = sizeof(PositionBufferType);
		particlePositionBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		particlePositionBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		particlePositionBufferDesc.MiscFlags = 0;
		particlePositionBufferDesc.StructureByteStride = 0;

		// Create the constant buffer pointer so we can access the vertex shader constant buffer from within this class.
		m_device->CreateBuffer(&particlePositionBufferDesc, NULL, &m_particlePositionBuffer);

		// *************************************** SET UP PARTICLE SCALE BUFFER ***************************************  
		// Setup the description of the dynamic matrix constant buffer that is in the geometry shader.
		particleScaleBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
		particleScaleBufferDesc.ByteWidth = sizeof(ScaleBufferType);
		particleScaleBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		particleScaleBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		particleScaleBufferDesc.MiscFlags = 0;
		particleScaleBufferDesc.StructureByteStride = 0;

		// Create the constant buffer pointer so we can access the vertex shader constant buffer from within this class.
		m_device->CreateBuffer(&particleScaleBufferDesc, NULL, &m_particleScaleBuffer);
	}

	void ParticleDepthShader::InitShader(WCHAR* vsFilename, WCHAR* gsFilename, WCHAR* psFilename)
	{
		// InitShader must be overwritten and it will load both vertex and pixel shaders + setup buffers
		InitShader(vsFilename, psFilename);

		// Load other required shaders.
		loadGeometryShader(gsFilename);
	}

	void ParticleDepthShader::SetShaderParameters(ID3D11DeviceContext* deviceContext, const XMMATRIX &world, const XMMATRIX &view, const XMMATRIX &projection, ParticleSystem* particleSystem)
	{
		HRESULT result;
		D3D11_MAPPED_SUBRESOURCE mappedResource;
		MatrixBufferType* dataPtr;
		PositionBufferType* positionPtr;
		ScaleBufferType* scalePtr;
		unsigned int bufferNumber;
		XMMATRIX tworld, tview, tproj;

		// ********************************** SET MATRIX DATA **************************************************

		// Transpose the matrices to prepare them for the shader.
		tworld = XMMatrixTranspose(world);
		tview = XMMatrixTranspose(view);
		tproj = XMMatrixTranspose(projection);

		// Lock the constant buffer so it can be written to.
		result = deviceContext->Map(m_matrixBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);

		// Get a pointer to the data in the constant buffer.
		dataPtr = (MatrixBufferType*)mappedResource.pData;

		// Copy the matrices into the constant buffer.
		dataPtr->world = tworld;// worldMatrix;
		dataPtr->view = tview;
		dataPtr->projection = tproj;

		// Unlock the constant buffer.
		deviceContext->Unmap(m_matrixBuffer, 0);

		// Set the position of the constant buffer in the vertex shader.
		bufferNumber = 0;

		// Now set the constant buffer in the vertex shader with the updated values.
		deviceContext->GSSetConstantBuffers(bufferNumber, 1, &m_matrixBuffer);

		// ********************************** SET PARTICLE POSITION DATA : VS ************************************************

		// Lock the constant buffer so it can be written to.
		result = deviceContext->Map(m_particlePositionBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);

		// Get a pointer to the data in the constant buffer.
		positionPtr = (PositionBufferType*)mappedResource.pData;

		// Copy position data into the constant buffer
		for (int i = 0; i < P_SYSTEMSIZE; ++i)
		{
			positionPtr->position[i] = particleSystem->GetParticleData()[i].GetPosition();
		}

		// Unlock the constant buffer.
		deviceContext->Unmap(m_particlePositionBuffer, 0);

		// Set the position of the constant buffer in the vertex shader.
		bufferNumber = 0;

		// Now set the constant buffer in the vertex shader with the updated values.
		deviceContext->VSSetConstantBuffers(bufferNumber, 1, &m_particlePositionBuffer);


		// ********************************** SET PARTICLE SCALE DATA : GS ************************************************

		// Lock the constant buffer so it can be written to.
		result = deviceContext->Map(m_particleScaleBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);

		// Get a pointer to the data in the constant buffer.
		scalePtr = (ScaleBufferType*)mappedResource.pData;

		// Copy scale data into the constant buffer
		for (int i = 0; i < P_SYSTEMSIZE; ++i)
		{
			scalePtr->scaleFactors[i] = particleSystem->GetParticleData()[i].GetScale();
		}

		// Unlock the constant buffer.
		deviceContext->Unmap(m_particleScaleBuffer, 0);

		// Set the position of the constant buffer in the vertex shader.
		bufferNumber = 1;

		// Now set the constant buffer in the vertex shader with the updated values.
		deviceContext->GSSetConstantBuffers(bufferNumber, 1, &m_particleScaleBuffer);
	}

	void ParticleDepthShader::Render(ID3D11DeviceContext* deviceContext, int indexCount)
	{
		// Base render function.
		BaseShader::Render(deviceContext, indexCount);
	}
}