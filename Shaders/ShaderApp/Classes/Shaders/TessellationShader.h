#ifndef _TESSELLATIONSHADER_H_
#define _TESSELLATIONSHADER_H_

#include "../DXFramework/BaseShader.h"
#include <../DXFramework/Light.h>
#include "ShaderUtils.h"

using namespace std;
using namespace DirectX;

// Tesselation shader

namespace ShaderApp
{
	class TessellationShader : public BaseShader
	{
	private:

// define camera distances used for applying tesselation
//#define CAMERADISTANCE_NEAR 75
//#define CAMERADISTANCE_FAR 100

	public:

		TessellationShader(ID3D11Device* device, HWND hwnd);
		~TessellationShader();

		void Update(XMFLOAT3 cameraPos, XMFLOAT3 tesselatedMeshPos);
		void SetShaderParameters(ID3D11DeviceContext* deviceContext, const XMMATRIX &world, const XMMATRIX &view, const XMMATRIX &projection,
			Light* light, ID3D11ShaderResourceView* texture, ID3D11ShaderResourceView* heightMap);
		void Render(ID3D11DeviceContext* deviceContext, int vertexCount);

		// Getters & Setters
		inline int GetTesselationFactor() const { return m_tesselationFactor; }
		inline void SetTesselationFactor(int newTesselationFactor) { m_tesselationFactor = newTesselationFactor; }
		
		inline bool GetCameraBasedTesselationDisabled() const { return m_cameraBasedTesselationDisabled; }
		inline void SetCameraBasedTesselationDisabled(bool newvalue) { m_cameraBasedTesselationDisabled = newvalue; }

	private:
		float CalcDistance(XMFLOAT3 cameraPos, XMFLOAT3 tesselatedMeshPos);

		void InitShader(WCHAR* vsFilename, WCHAR* psFilename);
		void InitShader(WCHAR* vsFilename, WCHAR* hsFilename, WCHAR* dsFilename, WCHAR* psFilename);

	private:
		// true if the tesselation is unaffected by camera position
		bool m_cameraBasedTesselationDisabled;
		// the tesselation factor applied to geometry
		int m_tesselationFactor;

		ID3D11Buffer* m_matrixBuffer;
		ID3D11Buffer* m_tesselationBuffer;
		ID3D11Buffer* m_lightBuffer;
		ID3D11SamplerState* m_sampleState;

		// limit values used to affect tesselation factor : difference between camera and mesh position
		const XMFLOAT3 k_cameraLimitNear;
		const XMFLOAT3 k_cameraLimitFar;

	};

}

#endif