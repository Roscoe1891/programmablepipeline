// Horizontal blur shader handler
// Loads horizontal blur shaders (vs and ps)
// Passes screen width to shaders, for sample coordinate calculation

#ifndef _BLURHORIZONTALSHADER_H_
#define _BLURHORIZONTALSHADER_H_

#include "../DXFramework/BaseShader.h"
#include "ShaderUtils.h"

using namespace std;
using namespace DirectX;

namespace ShaderApp
{
	class BlurHorizontalShader : public BaseShader
	{
	public:
		BlurHorizontalShader(ID3D11Device* device, HWND hwnd);
		~BlurHorizontalShader();

		void SetShaderParameters(ID3D11DeviceContext* deviceContext, const XMMATRIX &world, const XMMATRIX &view, const XMMATRIX &projection,
			ID3D11ShaderResourceView* texture, ID3D11ShaderResourceView* depthMap, BlurLevel blurLevel, float width);
		void Render(ID3D11DeviceContext* deviceContext, int vertexCount);

	private:
		void InitShader(WCHAR*, WCHAR*);

		ID3D11Buffer* m_matrixBuffer;
		ID3D11SamplerState* m_sampleState;
		ID3D11Buffer* m_ScreenWidthBuffer;
		ID3D11Buffer* m_BlurBuffer;

	};


}
#endif