#ifndef _RIPPLESHADER_H_
#define _RIPPLESHADER_H_

#include "../DXFramework/BaseShader.h"
#include <Lights/LightManager.h>
#include "ShaderUtils.h"

// Ripple Shader applies a wave effect through geometry

using namespace std;
using namespace DirectX;

namespace ShaderApp
{

	class RippleShader : public BaseShader
	{
	public:
		RippleShader(ID3D11Device* device, HWND hwnd);
		~RippleShader();

		void SetShaderParameters(ID3D11DeviceContext* deviceContext, const XMMATRIX &world, const XMMATRIX &view, const XMMATRIX &projection, ID3D11ShaderResourceView* texture, Light* light, XMFLOAT3 camera_pos);
		void Render(ID3D11DeviceContext* deviceContext, int vertexCount);
		void Update(float dt);

		inline float time_elapsed() const { return m_time_elapsed; }
		inline void set_time_elapsed(float new_time) { m_time_elapsed = new_time; }

		inline float GetWaveHeight() const { return m_wave_height; }
		inline void SetWaveHeight(float new_height) { m_wave_height = new_height; }

		inline float GetWaveFrequency() const { return m_wave_frequency; }
		inline void SetWaveFrequency(float new_freq) { m_wave_frequency = new_freq; }

	private:
		void InitShader(WCHAR*, WCHAR*);

		ID3D11Buffer* m_matrixBuffer;
		ID3D11SamplerState* m_sampleState;
		ID3D11Buffer* m_lightBuffer;
		ID3D11Buffer* m_timeBuffer;
		ID3D11Buffer* m_cameraBuffer;

		// wave variables
		float m_time_elapsed;
		float m_wave_height;
		float m_wave_frequency;
	};
}

#endif
