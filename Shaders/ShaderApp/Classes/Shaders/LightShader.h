#ifndef _LIGHTSHADER_H_
#define _LIGHTSHADER_H_

#include "../DXFramework/BaseShader.h"
#include <Lights/LightManager.h>
#include "ShaderUtils.h"

// Shader which handles lighting of geometry

using namespace std;
using namespace DirectX;

namespace ShaderApp
{
	class LightShader : public BaseShader
	{
	public:
		LightShader(ID3D11Device* device, HWND hwnd);
		~LightShader();

		void SetShaderParameters(ID3D11DeviceContext* deviceContext, const XMMATRIX &world, const XMMATRIX &view, const XMMATRIX &projection, ID3D11ShaderResourceView* texture, Light* light, XMFLOAT3 camera_pos);
		void Render(ID3D11DeviceContext* deviceContext, int vertexCount);

	private:
		void InitShader(WCHAR*, WCHAR*);

		ID3D11Buffer* m_matrixBuffer;
		ID3D11SamplerState* m_sampleState;
		ID3D11Buffer* m_lightBuffer;
		ID3D11Buffer* m_cameraBuffer;

	};

	
}

#endif