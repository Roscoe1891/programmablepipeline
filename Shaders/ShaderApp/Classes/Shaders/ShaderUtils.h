#ifndef _SHADERUTILS_H_
#define _SHADERUTILS_H_

#include "../DXFramework/BaseShader.h"
#include "../DXFramework/ParticleSystemMesh.h"
#include <Lights/LightManager.h>

namespace ShaderApp
{
	struct MatrixBufferType2
	{
		XMMATRIX world;
		XMMATRIX view;
		XMMATRIX projection;
		XMMATRIX lightView;
		XMMATRIX lightProjection;
	};

	struct RenderMatrixGroup
	{
		XMMATRIX worldMatrix;
		XMMATRIX viewMatrix;
		XMMATRIX projectionMatrix;
	};

	struct LightBufferType
	{
		XMFLOAT4 ambient[MAX_LIGHTS];
		XMFLOAT4 diffuse[MAX_LIGHTS];
		// Specular power is packed as the w component of position
		XMFLOAT4 position[MAX_LIGHTS];
		XMFLOAT4 specularColour[MAX_LIGHTS];
		// attenuation values should be packed as follows:
		// constant, linear, quadratic, range
		XMFLOAT4 attenuationValues[MAX_LIGHTS];
	};

	struct LightBufferType2
	{
		XMFLOAT3 position;
		float padding;
	};

	struct LightBufferType3
	{
		XMFLOAT4 ambientColour;
		XMFLOAT4 diffuseColour;
	};

	struct CameraBufferType
	{
		XMFLOAT3 cameraPosition;
		float padding;
	};

	struct TimeBufferType
	{
		float time;
		float height;
		float frequency;
		float padding;
	};

	struct ScreenWidthBufferType
	{
		float screenWidth;
		XMFLOAT3 padding;
	};

	struct ScreenHeightBufferType
	{
		float screenHeight;
		XMFLOAT3 padding;
	};

	struct PositionBufferType
	{
		XMFLOAT4 position[P_SYSTEMSIZE];
	};

	struct ScaleBufferType
	{
		float scaleFactors[P_SYSTEMSIZE];
	};

	struct TesselationBufferType
	{
		XMFLOAT4 tesselationEdges;
		XMFLOAT2 tesselationInside;
		XMFLOAT2 padding;
	};




#define MAX_BLUR_WEIGHTS 4

	// enumeration of the current level of blur to be applied
	enum BlurLevel { OFF, LIGHT, MEDIUM, HEAVY };

	// Gaussian Blur Weightings 
	static float BlurWeightings_StDev1[MAX_BLUR_WEIGHTS] =
	{ 0.3831f, 0.2418f, 0.0606f, 0.0059f };
	
	static float BlurWeightings_StDev3[MAX_BLUR_WEIGHTS] =
	{ 0.1749f, 0.1655f, 0.1403f, 0.1065f };

	static float BlurWeightings_StDev5[MAX_BLUR_WEIGHTS] =
	{ 0.1543f, 0.1513f, 0.1425f, 0.1290f };

	static float BlurWeightings_StDev7[MAX_BLUR_WEIGHTS] =
	{ 0.1487f, 0.1471f, 0.1427f, 0.1356f };

	struct BlurBufferType
	{
		XMFLOAT4 nearWeighting;
		XMFLOAT4 farWeighting;
	};


}

#endif