// Particle shader.h
// A shader that renders a particle system from points using the geometry shader stage

#ifndef _PARTICLESHADER_H_
#define _PARTICLESHADER_H_

#include "../DXFramework/BaseShader.h"
#include "../Scene/ParticleSystem.h"

using namespace std;
using namespace DirectX;

namespace ShaderApp
{
	class ParticleShader : public BaseShader
	{

	public:
		ParticleShader(ID3D11Device* device, HWND hwnd);
		~ParticleShader();

		void SetShaderParameters(ID3D11DeviceContext* deviceContext, const XMMATRIX &world, const XMMATRIX &view, const XMMATRIX &projection, 
			ID3D11ShaderResourceView* texture, ParticleSystem* particleSystem, Light* lights);
		void Render(ID3D11DeviceContext* deviceContext, int vertexCount);

	private:
		void InitShader(WCHAR* vsFilename, WCHAR* psFilename);
		void InitShader(WCHAR* vsFilename, WCHAR* gsFilename, WCHAR* psFilename);

	private:
		ID3D11Buffer* m_matrixBuffer;
		ID3D11Buffer* m_lightBuffer;
		ID3D11Buffer* m_particlePositionBuffer;
		ID3D11Buffer* m_particleScaleBuffer;

		ID3D11SamplerState* m_sampleState;
	};
}



#endif