#ifndef _TESSELATIONDEPTHSHADER_H_
#define _TESSELATIONDEPTHSHADER_H_

#include "../DXFramework/BaseShader.h"

// This shader is used to get a 'depth pass' from tesselated geometry
// This is necessray because doing a regular depth pass on a tesselation mesh would treat it as a flat plane
// So we need to calculate the tesselated vertex position, then work out the depth (see the domain shader file)

namespace ShaderApp
{
	class TesselationDepthShader : public BaseShader
	{

	private:
		struct TesselationBufferType
		{
			XMFLOAT4 tesselationEdges;
			XMFLOAT2 tesselationInside;
			XMFLOAT2 padding;
		};

	public:
		TesselationDepthShader(ID3D11Device* device, HWND hwnd);
		~TesselationDepthShader();

		void SetShaderParameters(ID3D11DeviceContext* deviceContext, const XMMATRIX &world, const XMMATRIX &view, const XMMATRIX &projection, ID3D11ShaderResourceView* heightMap, int tesselationFactor);
		void Render(ID3D11DeviceContext* deviceContext, int vertexCount);

	private:
		void InitShader(WCHAR* vsFilename, WCHAR* psFilename);
		void InitShader(WCHAR* vsFilename, WCHAR* hsFilename, WCHAR* dsFilename, WCHAR* psFilename);

		ID3D11Buffer* m_matrixBuffer;
		ID3D11Buffer* m_tesselationBuffer;
		ID3D11SamplerState* m_sampleState;
	};
}

#endif // !_TESSELATIONDEPTHSHADER_H_
