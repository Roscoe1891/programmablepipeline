#ifndef _PARTICLEDEPTHSHADER_H_
#define _PARTICLEDEPTHSHADER_H_

#include <../DXFramework/BaseShader.h>
#include <../DXFramework/ParticleSystemMesh.h>
#include <Scene/ParticleSystem.h>

// shader used for depth checks on the particle system

namespace ShaderApp
{
	class ParticleDepthShader : public BaseShader
	{
	public:
		ParticleDepthShader(ID3D11Device* device, HWND hwnd);
		~ParticleDepthShader();

		void SetShaderParameters(ID3D11DeviceContext* deviceContext, const XMMATRIX &world, const XMMATRIX &view, const XMMATRIX &projection, ParticleSystem* particleSystem);
		void Render(ID3D11DeviceContext* deviceContext, int indexCount);

	private:
		void InitShader(WCHAR* vsFilename, WCHAR* psFilename);
		void InitShader(WCHAR* vsFilename, WCHAR* gsFilename, WCHAR* psFilename);

		ID3D11Buffer* m_matrixBuffer;
		ID3D11Buffer* m_particlePositionBuffer;
		ID3D11Buffer* m_particleScaleBuffer;

	};
}

#endif