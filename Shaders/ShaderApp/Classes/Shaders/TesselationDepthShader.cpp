#include "TesselationDepthShader.h"

namespace ShaderApp
{
	TesselationDepthShader::TesselationDepthShader(ID3D11Device* device, HWND hwnd) : BaseShader(device, hwnd)
	{
		m_matrixBuffer = nullptr;
		m_tesselationBuffer = nullptr;

		InitShader(L"ShaderFiles/tessellation_vs.hlsl", L"ShaderFiles/tessellation_hs.hlsl", L"ShaderFiles/tessellationDepth_ds.hlsl", L"ShaderFiles/depth_ps.hlsl");
	}

	TesselationDepthShader::~TesselationDepthShader()
	{
		// Release the matrix buffer.
		if (m_matrixBuffer)
		{
			m_matrixBuffer->Release();
			m_matrixBuffer = nullptr;
		}

		// Release the tesselation buffer.
		if (m_tesselationBuffer)
		{
			m_tesselationBuffer->Release();
			m_tesselationBuffer = nullptr;
		}

		// Release the sampler state.
		if (m_sampleState)
		{
			m_sampleState->Release();
			m_sampleState = 0;
		}

		// Release the layout
		if (m_layout)
		{
			m_layout->Release();
			m_layout = 0;
		}

		//Release base shader components
		BaseShader::~BaseShader();
	}

	void TesselationDepthShader::InitShader(WCHAR* vsFilename, WCHAR* psFilename)
	{
		D3D11_BUFFER_DESC matrixBufferDesc;
		D3D11_SAMPLER_DESC samplerDesc;

		// Load (+ compile) shader files
		loadVertexShader(vsFilename);
		loadPixelShader(psFilename);

		// Setup the description of the dynamic matrix constant buffer that is in the vertex shader.
		matrixBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
		matrixBufferDesc.ByteWidth = sizeof(MatrixBufferType);
		matrixBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		matrixBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		matrixBufferDesc.MiscFlags = 0;
		matrixBufferDesc.StructureByteStride = 0;

		// Create the constant buffer pointer so we can access the vertex shader constant buffer from within this class.
		m_device->CreateBuffer(&matrixBufferDesc, NULL, &m_matrixBuffer);

		// Create a texture sampler state description.
		samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
		samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_MIRROR;
		samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_MIRROR;
		samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_MIRROR;
		samplerDesc.MipLODBias = 0.0f;
		samplerDesc.MaxAnisotropy = 1;
		samplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
		samplerDesc.BorderColor[0] = 0;
		samplerDesc.BorderColor[1] = 0;
		samplerDesc.BorderColor[2] = 0;
		samplerDesc.BorderColor[3] = 0;
		samplerDesc.MinLOD = 0;
		samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;

		// Create the texture sampler state.
		m_device->CreateSamplerState(&samplerDesc, &m_sampleState);

	}

	void TesselationDepthShader::InitShader(WCHAR* vsFilename, WCHAR* hsFilename, WCHAR* dsFilename, WCHAR* psFilename)
	{
		// InitShader must be overwritten and it will load both vertex and pixel shaders + setup buffers
		InitShader(vsFilename, psFilename);

		// Load other required shaders.
		loadHullShader(hsFilename);
		loadDomainShader(dsFilename);

		// ********************************************* TESSELATION BUFFER SET UP ***********************************************

		D3D11_BUFFER_DESC tesselationBufferDesc;
		// Setup the description of the tesselation constant buffer that is in the hull shader.
		tesselationBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
		tesselationBufferDesc.ByteWidth = sizeof(TesselationBufferType);
		tesselationBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		tesselationBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		tesselationBufferDesc.MiscFlags = 0;
		tesselationBufferDesc.StructureByteStride = 0;

		// Create the constant buffer pointer so we can access the vertex shader constant buffer from within this class.
		m_device->CreateBuffer(&tesselationBufferDesc, NULL, &m_tesselationBuffer);

	}

	void TesselationDepthShader::SetShaderParameters(ID3D11DeviceContext* deviceContext, const XMMATRIX &world, const XMMATRIX &view, const XMMATRIX &projection, 
		ID3D11ShaderResourceView* heightMap, int tesselationFactor)
	{
		HRESULT result;
		D3D11_MAPPED_SUBRESOURCE mappedResource;
		MatrixBufferType* dataPtr;
		TesselationBufferType* tesselationPtr;
		unsigned int bufferNumber;
		XMMATRIX tworld, tview, tproj;

		// Transpose the matrices to prepare them for the shader.
		tworld = XMMatrixTranspose(world);
		tview = XMMatrixTranspose(view);
		tproj = XMMatrixTranspose(projection);

		// ****************************** MATRIX BUFFER : DOMAIN SHADER *************************************

		// Lock the constant buffer so it can be written to.
		result = deviceContext->Map(m_matrixBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);

		// Get a pointer to the data in the constant buffer.
		dataPtr = (MatrixBufferType*)mappedResource.pData;

		// Copy the matrices into the constant buffer.
		dataPtr->world = tworld;// worldMatrix;
		dataPtr->view = tview;
		dataPtr->projection = tproj;

		// Unlock the constant buffer.
		deviceContext->Unmap(m_matrixBuffer, 0);

		// Set the position of the constant buffer in the vertex shader.
		bufferNumber = 0;

		// Now set the constant buffer in the domain shader with the updated values.
		deviceContext->DSSetConstantBuffers(bufferNumber, 1, &m_matrixBuffer);

		// ****************************** TESSELATION BUFFER : HULL SHADER **********************************

		// Lock the constant buffer so it can be written to.
		result = deviceContext->Map(m_tesselationBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);

		// Get a pointer to the data in the constant buffer.
		tesselationPtr = (TesselationBufferType*)mappedResource.pData;

		// Copy the data into the constant buffer.
		tesselationPtr->tesselationEdges.x = (float)tesselationFactor; //32.f;
		tesselationPtr->tesselationEdges.y = (float)tesselationFactor; //32.f; 
		tesselationPtr->tesselationEdges.z = (float)tesselationFactor; //32.f;
		tesselationPtr->tesselationEdges.w = (float)tesselationFactor; //32.f;

		tesselationPtr->tesselationInside.x = (float)tesselationFactor; //32.f;
		tesselationPtr->tesselationInside.y = (float)tesselationFactor; //32.f;

		tesselationPtr->padding = XMFLOAT2(0.f, 0.f);

		// Unlock the constant buffer.
		deviceContext->Unmap(m_tesselationBuffer, 0);

		// Set the position of the constant buffer in the hull shader.
		bufferNumber = 0;

		// Now set the constant buffer in the hull shader with the updated values.
		deviceContext->HSSetConstantBuffers(bufferNumber, 1, &m_tesselationBuffer);

		// *************************** SET RESOURCES *******************************************************

		// Set shader texture resource in the domain shader.
		deviceContext->DSSetShaderResources(0, 1, &heightMap);

	}

	void TesselationDepthShader::Render(ID3D11DeviceContext* deviceContext, int vertexCount)
	{
		deviceContext->DSSetSamplers(0, 1, &m_sampleState);

		// Base render function.
		BaseShader::Render(deviceContext, vertexCount);
	}

	
}