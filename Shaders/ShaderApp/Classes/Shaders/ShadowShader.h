#ifndef _SHADOWSHADER_H_
#define _SHADOWSHADER_H_

#include "../DXFramework/BaseShader.h"
#include "../DXFramework/Light.h"
#include "../DXFramework/Camera.h"
#include "ShaderUtils.h"

// Apply shadowed lighting to rendered geometry

using namespace std;
using namespace DirectX;

namespace ShaderApp
{

	class ShadowShader : public BaseShader
	{

	public:

		ShadowShader(ID3D11Device* device, HWND hwnd);
		~ShadowShader();

		void SetShaderParameters(ID3D11DeviceContext* deviceContext, const XMMATRIX &world, const XMMATRIX &view, const XMMATRIX &projection,
			ID3D11ShaderResourceView* texture, ID3D11ShaderResourceView*depthMap, Light* light, Camera* camera);
		void Render(ID3D11DeviceContext* deviceContext, int vertexCount);

	private:
		void InitShader(WCHAR*, WCHAR*);

	private:
		ID3D11Buffer* m_matrixBuffer;
		ID3D11SamplerState* m_sampleState;
		ID3D11SamplerState* m_sampleStateClamp;
		ID3D11Buffer* m_LightBuffer;
		ID3D11Buffer* m_LightBuffer2;
		ID3D11Buffer* m_cameraBuffer;

	};
}

#endif