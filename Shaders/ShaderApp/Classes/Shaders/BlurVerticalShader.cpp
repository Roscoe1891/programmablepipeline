// Vertical blur shader
#include "BlurVerticalShader.h"

namespace ShaderApp
{
	BlurVerticalShader::BlurVerticalShader(ID3D11Device* device, HWND hwnd) : BaseShader(device, hwnd)
	{
		InitShader(L"ShaderFiles/blurVertical_vs.hlsl", L"ShaderFiles/blurVertical_ps.hlsl");
	}

	BlurVerticalShader::~BlurVerticalShader()
	{
		// Release the sampler state.
		if (m_sampleState)
		{
			m_sampleState->Release();
			m_sampleState = 0;
		}

		// Release the matrix constant buffer.
		if (m_matrixBuffer)
		{
			m_matrixBuffer->Release();
			m_matrixBuffer = 0;
		}

		// Release the layout.
		if (m_layout)
		{
			m_layout->Release();
			m_layout = 0;
		}

		// Release the light constant buffer.
		if (m_ScreenHeightBuffer)
		{
			m_ScreenHeightBuffer->Release();
			m_ScreenHeightBuffer = 0;
		}

		// Release the blur constant buffer
		if (m_BlurBuffer)
		{
			m_BlurBuffer->Release();
			m_BlurBuffer = 0;
		}

		//Release base shader components
		BaseShader::~BaseShader();
	}


	void BlurVerticalShader::InitShader(WCHAR* vsFilename, WCHAR* psFilename)
	{
		D3D11_BUFFER_DESC matrixBufferDesc;
		D3D11_SAMPLER_DESC samplerDesc;
		D3D11_BUFFER_DESC screenHeightBufferDesc;
		D3D11_BUFFER_DESC blurBufferDesc;

		// Load (+ compile) shader files
		loadVertexShader(vsFilename);
		loadPixelShader(psFilename);

		// Setup the description of the dynamic matrix constant buffer that is in the vertex shader.
		matrixBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
		matrixBufferDesc.ByteWidth = sizeof(MatrixBufferType);
		matrixBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		matrixBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		matrixBufferDesc.MiscFlags = 0;
		matrixBufferDesc.StructureByteStride = 0;

		// Create the constant buffer pointer so we can access the vertex shader constant buffer from within this class.
		m_device->CreateBuffer(&matrixBufferDesc, NULL, &m_matrixBuffer);

		// Create a texture sampler state description.
		samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
		samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_MIRROR;
		samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_MIRROR;
		samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_MIRROR;
		samplerDesc.MipLODBias = 0.0f;
		samplerDesc.MaxAnisotropy = 1;
		samplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
		samplerDesc.BorderColor[0] = 0;
		samplerDesc.BorderColor[1] = 0;
		samplerDesc.BorderColor[2] = 0;
		samplerDesc.BorderColor[3] = 0;
		samplerDesc.MinLOD = 0;
		samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;

		// Create the texture sampler state.
		m_device->CreateSamplerState(&samplerDesc, &m_sampleState);

		// SCREEN SIZE CONSTANT BUFFER : VS

		// Setup the description of the screen size constant buffer that is in the vertex shader.
		screenHeightBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
		screenHeightBufferDesc.ByteWidth = sizeof(ScreenHeightBufferType);
		screenHeightBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		screenHeightBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		screenHeightBufferDesc.MiscFlags = 0;
		screenHeightBufferDesc.StructureByteStride = 0;

		// Create the screen size constant buffer pointer so we can access the vertex shader constant buffer from within this class.
		m_device->CreateBuffer(&screenHeightBufferDesc, NULL, &m_ScreenHeightBuffer);

		// BLUR CONSTANT BUFFER : PS

		// Setup the description of the blur constant buffer that is in the pixel shader.
		blurBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
		blurBufferDesc.ByteWidth = sizeof(BlurBufferType);
		blurBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		blurBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		blurBufferDesc.MiscFlags = 0;
		blurBufferDesc.StructureByteStride = 0;

		// Create the blur constant buffer pointer so we can access the pixel shader constant buffer from within this class.
		m_device->CreateBuffer(&blurBufferDesc, NULL, &m_BlurBuffer);
	}


	void BlurVerticalShader::SetShaderParameters(ID3D11DeviceContext* deviceContext, const XMMATRIX &worldMatrix, const XMMATRIX &viewMatrix, const XMMATRIX &projectionMatrix,
		ID3D11ShaderResourceView* texture, ID3D11ShaderResourceView* depthMap, BlurLevel blurLevel, float height)
	{
		HRESULT result;
		D3D11_MAPPED_SUBRESOURCE mappedResource;
		MatrixBufferType* dataPtr;
		ScreenHeightBufferType* heightPtr;
		BlurBufferType* blurPtr;
		unsigned int bufferNumber;
		XMMATRIX tworld, tview, tproj;

		// ******************************* SET MATRIX BUFFER : VS ********************************************

		// Transpose the matrices to prepare them for the shader.
		tworld = XMMatrixTranspose(worldMatrix);
		tview = XMMatrixTranspose(viewMatrix);
		tproj = XMMatrixTranspose(projectionMatrix);

		// Lock the constant buffer so it can be written to.
		result = deviceContext->Map(m_matrixBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);

		// Get a pointer to the data in the constant buffer.
		dataPtr = (MatrixBufferType*)mappedResource.pData;

		// Copy the matrices into the constant buffer.
		dataPtr->world = tworld;// worldMatrix;
		dataPtr->view = tview;
		dataPtr->projection = tproj;

		// Unlock the constant buffer.
		deviceContext->Unmap(m_matrixBuffer, 0);

		// Set the position of the constant buffer in the vertex shader.
		bufferNumber = 0;

		// Now set the constant buffer in the vertex shader with the updated values.
		deviceContext->VSSetConstantBuffers(bufferNumber, 1, &m_matrixBuffer);

		// ******************************* SET WIDTH BUFFER : VS ********************************************

		deviceContext->Map(m_ScreenHeightBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
		heightPtr = (ScreenHeightBufferType*)mappedResource.pData;
		heightPtr->screenHeight = height;
		heightPtr->padding = XMFLOAT3(1.0f, 1.f, 1.f);
		deviceContext->Unmap(m_ScreenHeightBuffer, 0);
		bufferNumber = 1;
		deviceContext->VSSetConstantBuffers(bufferNumber, 1, &m_ScreenHeightBuffer);

		// ******************************* SET BLUR BUFFER : PS ********************************************
		deviceContext->Map(m_BlurBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
		blurPtr = (BlurBufferType*)mappedResource.pData;
		
		// Set values depending on current blur level
		switch (blurLevel)
		{

		case BlurLevel::LIGHT:
			blurPtr->nearWeighting.x = BlurWeightings_StDev1[0];
			blurPtr->nearWeighting.y = BlurWeightings_StDev1[1];
			blurPtr->nearWeighting.z = BlurWeightings_StDev1[2];
			blurPtr->nearWeighting.w = BlurWeightings_StDev1[3];

			blurPtr->farWeighting.x = BlurWeightings_StDev3[0];
			blurPtr->farWeighting.y = BlurWeightings_StDev3[1];
			blurPtr->farWeighting.z = BlurWeightings_StDev3[2];
			blurPtr->farWeighting.w = BlurWeightings_StDev3[3];

			break;

		case BlurLevel::MEDIUM:
			blurPtr->nearWeighting.x = BlurWeightings_StDev3[0];
			blurPtr->nearWeighting.y = BlurWeightings_StDev3[1];
			blurPtr->nearWeighting.z = BlurWeightings_StDev3[2];
			blurPtr->nearWeighting.w = BlurWeightings_StDev3[3];

			blurPtr->farWeighting.x = BlurWeightings_StDev5[0];
			blurPtr->farWeighting.y = BlurWeightings_StDev5[1];
			blurPtr->farWeighting.z = BlurWeightings_StDev5[2];
			blurPtr->farWeighting.w = BlurWeightings_StDev5[3];

			break;

		case BlurLevel::HEAVY:
			blurPtr->nearWeighting.x = BlurWeightings_StDev5[0];
			blurPtr->nearWeighting.y = BlurWeightings_StDev5[1];
			blurPtr->nearWeighting.z = BlurWeightings_StDev5[2];
			blurPtr->nearWeighting.w = BlurWeightings_StDev5[3];

			blurPtr->farWeighting.x = BlurWeightings_StDev7[0];
			blurPtr->farWeighting.y = BlurWeightings_StDev7[1];
			blurPtr->farWeighting.z = BlurWeightings_StDev7[2];
			blurPtr->farWeighting.w = BlurWeightings_StDev7[3];

			break;
		}

		deviceContext->Unmap(m_BlurBuffer, 0);
		bufferNumber = 0;
		deviceContext->PSSetConstantBuffers(bufferNumber, 1, &m_BlurBuffer);

		// ******************************* SET RESOURCES ********************************************

		// Set shader texture resource in the pixel shader.
		deviceContext->PSSetShaderResources(0, 1, &texture);
		deviceContext->PSSetShaderResources(1, 1, &depthMap);

	}

	void BlurVerticalShader::Render(ID3D11DeviceContext* deviceContext, int indexCount)
	{
		// Set the sampler state in the pixel shader.
		deviceContext->PSSetSamplers(0, 1, &m_sampleState);

		// Base render function.
		BaseShader::Render(deviceContext, indexCount);
	}

}

