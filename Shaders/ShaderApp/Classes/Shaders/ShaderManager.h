#ifndef _SHADERMANAGER_H_
#define _SHADERMANAGER_H_

#include "../DXFramework/Input.h"
#include "TextureShader.h"
#include "LightShader.h"
#include "DepthShader.h"
#include "ShadowShader.h"
#include "RippleShader.h"
#include "TessellationShader.h"
#include "TesselationDepthShader.h"
#include "BlurHorizontalShader.h"
#include "BlurVerticalShader.h"

namespace ShaderApp
{
	// forward declarations
	class ParticleShader;
	class ParticleDepthShader;
	class ParticleSystem;

	class ShaderManager
	{
	public:
		enum ShaderType { TEXTURE, LIGHT, DEPTH, SHADOW, RIPPLE, PARTICLE, PARTICLE_DEPTH, TESSELATION, TESSELATION_DEPTH, BLUR_HORIZONTAL, BLUR_VERTICAL, NUM_SHADERS };

		ShaderManager();
		~ShaderManager();
		// Create the shaders
		void CreateShaders(ID3D11Device* device, ID3D11DeviceContext* deviceContext, HWND hwnd);
		// Assign shaders to the shader array
		void AssignShaderRefs();
		// Update any time or input -senstive shader
		void Update(float dt, Input* input, XMFLOAT3 cameraPos, XMFLOAT3 terrainPos);
		// Free memory
		void CleanUp();

		// SET SHADER PARAMETERS
		// Texture / Tesselation_Depth Override
		void SetShaderParameters(ShaderType shaderType, const XMMATRIX &world, const XMMATRIX &view, const XMMATRIX &projection, ID3D11ShaderResourceView* texture);
		// Light / Ripple Override
		void SetShaderParameters(ShaderType shaderType, const XMMATRIX &world, const XMMATRIX &view, const XMMATRIX &projection, ID3D11ShaderResourceView* texture, Light* light, XMFLOAT3 cameraPos);
		// Depth Override
		void SetShaderParameters(ShaderType shaderType, const XMMATRIX &world, const XMMATRIX &view, const XMMATRIX &projection);
		// Shadow Override
		void SetShaderParameters(ShaderType shaderType, const XMMATRIX &world, const XMMATRIX &view, const XMMATRIX &projection,
			ID3D11ShaderResourceView* texture, ID3D11ShaderResourceView*depthMap, Light* light, Camera* camera);
		// Particle Override
		void SetShaderParameters(ShaderType shaderType, const XMMATRIX &world, const XMMATRIX &view, const XMMATRIX &projection,
			ID3D11ShaderResourceView* texture, ParticleSystem* particleSystem, Light* lights);
		// Particle Depth Override
		void SetShaderParameters(ShaderType shaderType, const XMMATRIX &world, const XMMATRIX &view, const XMMATRIX &projection, ParticleSystem* particleSystem);
		// Tesselation Override
		void SetShaderParameters(ShaderType shaderType, const XMMATRIX &world, const XMMATRIX &view, const XMMATRIX &projection,
			Light* light, ID3D11ShaderResourceView* texture, ID3D11ShaderResourceView* heightMap);
		// Blur Horizontal / Blur Vertical Override
		// Screen Dimension will be treated as width or heiht depending on shadertype specified
		void SetShaderParameters(ShaderType shaderType, const XMMATRIX &world, const XMMATRIX &view, const XMMATRIX &projection, 
			ID3D11ShaderResourceView* texture, ID3D11ShaderResourceView* depthMap, float screen_dimension);

		// Specify shader type for rendering
		void Render(ShaderType shaderType, int vertexCount);
		 
		// Getters & Setters
		inline TextureShader* GetTextureShader() { return m_TextureShader; }
		inline TessellationShader* GetTesselationShader() { return m_TesselationShader; }
		inline RippleShader* GetRippleShader() { return m_RippleShader; }

		inline BlurLevel GetBlurLevel() { return m_BlurLevel; }
		inline void SetBlurLevel(BlurLevel newBlurLevel) { m_BlurLevel = newBlurLevel; }

	private:
		// device pointer
		ID3D11Device* m_Device;
		// device context pointer
		ID3D11DeviceContext* m_DeviceContext;

		// access array for shaders 
		BaseShader* m_shaders[NUM_SHADERS];

		// Current Blur Level
		BlurLevel m_BlurLevel;

		// The shaders
		TextureShader* m_TextureShader;
		LightShader* m_LightShader;
		DepthShader* m_DepthShader;
		ShadowShader* m_ShadowShader;
		RippleShader* m_RippleShader;
		ParticleShader* m_ParticleShader;
		ParticleDepthShader* m_ParticleDepthShader;
		TessellationShader* m_TesselationShader;
		TesselationDepthShader* m_TesselationDepthShader;
		BlurHorizontalShader* m_BlurHorizontalShader;
		BlurVerticalShader* m_BlurVerticalShader;
	};
}

#endif
