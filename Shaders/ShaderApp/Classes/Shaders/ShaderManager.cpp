#include "ShaderManager.h"
#include "ParticleShader.h"
#include "ParticleDepthShader.h"

namespace ShaderApp
{
	ShaderManager::ShaderManager() : 
		m_TextureShader(nullptr),
		m_LightShader(nullptr),
		m_DepthShader(nullptr),
		m_ShadowShader(nullptr),
		m_RippleShader(nullptr),
		m_ParticleShader(nullptr),
		m_ParticleDepthShader(nullptr),
		m_TesselationShader(nullptr),
		m_TesselationDepthShader(nullptr),
		m_BlurHorizontalShader(nullptr),
		m_BlurVerticalShader(nullptr)
	{
		m_BlurLevel = BlurLevel::LIGHT;
	}

	ShaderManager::~ShaderManager()
	{
		CleanUp();
	}

	void ShaderManager::CreateShaders(ID3D11Device* device, ID3D11DeviceContext* deviceContext, HWND hwnd)
	{	
		m_Device = device;
		m_DeviceContext = deviceContext;

		m_TextureShader = new TextureShader(m_Device, hwnd);
		m_LightShader = new LightShader(m_Device, hwnd);
		m_DepthShader = new DepthShader(m_Device, hwnd);
		m_ShadowShader = new ShadowShader(m_Device, hwnd);
		m_RippleShader = new RippleShader(m_Device, hwnd);
		m_ParticleShader = new ParticleShader(m_Device, hwnd);
		m_ParticleDepthShader = new ParticleDepthShader(m_Device, hwnd);
		m_TesselationShader = new TessellationShader(m_Device, hwnd);
		m_TesselationDepthShader = new TesselationDepthShader(m_Device, hwnd);
		m_BlurHorizontalShader = new BlurHorizontalShader(m_Device, hwnd);
		m_BlurVerticalShader = new BlurVerticalShader(m_Device, hwnd);

		AssignShaderRefs();

		
	}

	void ShaderManager::AssignShaderRefs()
	{
		m_shaders[TEXTURE] = m_TextureShader;
		m_shaders[LIGHT] = m_LightShader;
		m_shaders[DEPTH] = m_DepthShader;
		m_shaders[SHADOW] = m_ShadowShader;
		m_shaders[RIPPLE] = m_RippleShader;
		m_shaders[PARTICLE] = m_ParticleShader;
		m_shaders[PARTICLE_DEPTH] = m_ParticleDepthShader;
		m_shaders[TESSELATION] = m_TesselationShader;
		m_shaders[TESSELATION_DEPTH] = m_TesselationDepthShader;
		m_shaders[BLUR_HORIZONTAL] = m_BlurHorizontalShader;
		m_shaders[BLUR_VERTICAL] = m_BlurVerticalShader;
	}

	void ShaderManager::Update(float dt, Input* input, XMFLOAT3 cameraPos, XMFLOAT3 terrainPos)
	{
		// Update Ripple shader
		m_RippleShader->Update(dt);

		// Update Tesselation shader
		m_TesselationShader->Update(cameraPos, terrainPos);

	}

	void ShaderManager::CleanUp()
	{
		// Release Texture Shader
		if (m_TextureShader)
		{
			delete m_TextureShader;
			m_TextureShader = nullptr;
		}

		// Release Light Shader
		if (m_LightShader)
		{
			delete m_LightShader;
			m_LightShader = nullptr;
		}

		// Release Depth Shader
		if (m_DepthShader)
		{
			delete m_DepthShader;
			m_DepthShader = nullptr;
		}

		// Release Shadow Shader
		if (m_ShadowShader)
		{
			delete m_ShadowShader;
			m_ShadowShader = nullptr;
		}

		// Release Ripple Shader
		if (m_RippleShader)
		{
			delete m_RippleShader;
			m_RippleShader = nullptr;
		}

		// Release Particle Shader
		if (m_ParticleShader)
		{
			delete m_ParticleShader;
			m_ParticleShader = nullptr;
		}

		// Release Particle Depth Shader
		if (m_ParticleDepthShader)
		{
			delete m_ParticleDepthShader;
			m_ParticleDepthShader = nullptr;
		}

		// Release Tesselation Shader
		if (m_TesselationShader)
		{
			delete m_TesselationShader;
			m_TesselationShader = nullptr;
		}

		// Release Tesselation Depth Shader
		if (m_TesselationDepthShader)
		{
			delete m_TesselationDepthShader;
			m_TesselationDepthShader = nullptr;
		}

		// Release Horizontal Blur shader
		if (m_BlurHorizontalShader)
		{
			delete m_BlurHorizontalShader;
			m_BlurHorizontalShader = nullptr;
		}

		// Release Vertical Blur shader
		if (m_BlurVerticalShader)
		{
			delete m_BlurVerticalShader;
			m_BlurVerticalShader = nullptr;
		}
	}

	void ShaderManager::SetShaderParameters(ShaderType shaderType, const XMMATRIX &world, const XMMATRIX &view, const XMMATRIX &projection, ID3D11ShaderResourceView* texture)
	{
		// validity check
		switch (shaderType)
		{
		case TEXTURE:
			m_TextureShader->SetShaderParameters(m_DeviceContext, world, view, projection, texture);
			break;
		case TESSELATION_DEPTH:
			m_TesselationDepthShader->SetShaderParameters(m_DeviceContext, world, view, projection, texture, m_TesselationShader->GetTesselationFactor());
			break;
		default:
			break;
		}
	}

	void ShaderManager::SetShaderParameters(ShaderType shaderType, const XMMATRIX &world, const XMMATRIX &view, const XMMATRIX &projection, ID3D11ShaderResourceView* texture, Light* light, XMFLOAT3 cameraPos)
	{
		// validity check
		switch(shaderType)
		{
		case LIGHT:
			m_LightShader->SetShaderParameters(m_DeviceContext, world, view, projection, texture, light, cameraPos);
			break;
		case RIPPLE:
			m_RippleShader->SetShaderParameters(m_DeviceContext, world, view, projection, texture, light, cameraPos);
			break;
		default:
			break;
		}
	}
	
	void ShaderManager::SetShaderParameters(ShaderType shaderType, const XMMATRIX &world, const XMMATRIX &view, const XMMATRIX &projection)
	{
		// validity check
		if (shaderType == DEPTH)
		{
			m_DepthShader->SetShaderParameters(m_DeviceContext, world, view, projection);
		}
	}

	void ShaderManager::SetShaderParameters(ShaderType shaderType, const XMMATRIX &world, const XMMATRIX &view, const XMMATRIX &projection,
		ID3D11ShaderResourceView* texture, ID3D11ShaderResourceView*depthMap, Light* light, Camera* camera)
	{
		// validity check
		if (shaderType == SHADOW)
		{
			m_ShadowShader->SetShaderParameters(m_DeviceContext, world, view, projection, texture, depthMap, light, camera);
		}
	}

	void ShaderManager::SetShaderParameters(ShaderType shaderType, const XMMATRIX &world, const XMMATRIX &view, const XMMATRIX &projection,
		ID3D11ShaderResourceView* texture, ParticleSystem* particleSystem, Light* lights)
	{
		// validity check
		if (shaderType == PARTICLE)
		{
			m_ParticleShader->SetShaderParameters(m_DeviceContext, world, view, projection, texture, particleSystem, lights);
		}
	}

	void ShaderManager::SetShaderParameters(ShaderType shaderType, const XMMATRIX &world, const XMMATRIX &view, const XMMATRIX &projection, ParticleSystem* particleSystem)
	{
		// validity check
		if (shaderType == PARTICLE_DEPTH)
		{
			m_ParticleDepthShader->SetShaderParameters(m_DeviceContext, world, view, projection, particleSystem);
		}
	}

	void ShaderManager::SetShaderParameters(ShaderType shaderType, const XMMATRIX &world, const XMMATRIX &view, const XMMATRIX &projection,
		Light* light, ID3D11ShaderResourceView* texture, ID3D11ShaderResourceView* heightMap)
	{
		// validity check
		if (shaderType == TESSELATION)
		{
			m_TesselationShader->SetShaderParameters(m_DeviceContext, world, view, projection, light, texture, heightMap);
		}
	}

	void ShaderManager::SetShaderParameters(ShaderType shaderType, const XMMATRIX &world, const XMMATRIX &view, const XMMATRIX &projection, 
		ID3D11ShaderResourceView* texture, ID3D11ShaderResourceView* depthMap, float screen_dimension)
	{
		switch (shaderType)
		{
		case BLUR_HORIZONTAL:
			m_BlurHorizontalShader->SetShaderParameters(m_DeviceContext, world, view, projection, texture, depthMap, m_BlurLevel, screen_dimension);
			break;
		case BLUR_VERTICAL:
			m_BlurVerticalShader->SetShaderParameters(m_DeviceContext, world, view, projection, texture, depthMap, m_BlurLevel, screen_dimension);
			break;
		default:
			break;
		}
	}

	void ShaderManager::Render(ShaderType shaderType, int vertexCount)
	{
		// check type and call appropriate render function
		switch (shaderType)
		{
		case TEXTURE:
			m_TextureShader->Render(m_DeviceContext, vertexCount);
			break;
		case LIGHT:
			m_LightShader->Render(m_DeviceContext, vertexCount);
			break;
		case DEPTH:
			m_DepthShader->Render(m_DeviceContext, vertexCount);
			break;
		case SHADOW:
			m_ShadowShader->Render(m_DeviceContext, vertexCount);
			break;
		case RIPPLE:
			m_RippleShader->Render(m_DeviceContext, vertexCount);
			break;
		case PARTICLE:
			m_ParticleShader->Render(m_DeviceContext, vertexCount);
			break;
		case PARTICLE_DEPTH:
			m_ParticleDepthShader->Render(m_DeviceContext, vertexCount);
			break;
		case TESSELATION:
			m_TesselationShader->Render(m_DeviceContext, vertexCount);
			break;
		case TESSELATION_DEPTH:
			m_TesselationDepthShader->Render(m_DeviceContext, vertexCount);
			break;
		case BLUR_HORIZONTAL:
			m_BlurHorizontalShader->Render(m_DeviceContext, vertexCount);
			break;
		case BLUR_VERTICAL:
			m_BlurVerticalShader->Render(m_DeviceContext, vertexCount);
			break;

		}
	}

}