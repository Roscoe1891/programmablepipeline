// Vertical blur shader handler
// Loads vertical blur shaders (vs and ps)
// Passes screen height to shaders, for sample coordinate calculation
#ifndef _BLURVERTICALSHADER_H_
#define _BLURVERTICALSHADER_H_

#include "../DXFramework/BaseShader.h"
#include "ShaderUtils.h"

using namespace std;
using namespace DirectX;

namespace ShaderApp
{
	class BlurVerticalShader : public BaseShader
	{
	public:

		BlurVerticalShader(ID3D11Device* device, HWND hwnd);
		~BlurVerticalShader();

		void SetShaderParameters(ID3D11DeviceContext* deviceContext, const XMMATRIX &world, const XMMATRIX &view, const XMMATRIX &projection,
			ID3D11ShaderResourceView* texture, ID3D11ShaderResourceView* depthMap, BlurLevel blurLevel, float width);
		void Render(ID3D11DeviceContext* deviceContext, int vertexCount);

	private:
		void InitShader(WCHAR*, WCHAR*);

		ID3D11Buffer* m_matrixBuffer;
		ID3D11SamplerState* m_sampleState;
		ID3D11Buffer* m_ScreenHeightBuffer;
		ID3D11Buffer* m_BlurBuffer;

	};
}



#endif