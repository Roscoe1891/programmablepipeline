// tessellation shader.cpp
#include "tessellationshader.h"

namespace ShaderApp
{
	TessellationShader::TessellationShader(ID3D11Device* device, HWND hwnd) : BaseShader(device, hwnd),
		k_cameraLimitNear(XMFLOAT3(125.f, 75.f, 125.f)),
		k_cameraLimitFar(XMFLOAT3(150.f, 100.f, 150.f))
	{
		InitShader(L"ShaderFiles/tessellation_vs.hlsl", L"ShaderFiles/tessellation_hs.hlsl", L"ShaderFiles/tessellation_ds.hlsl", L"ShaderFiles/tessellation_ps.hlsl");
	}


	TessellationShader::~TessellationShader()
	{
		// Release the sampler state.
		if (m_sampleState)
		{
			m_sampleState->Release();
			m_sampleState = 0;
		}

		// Release the matrix constant buffer.
		if (m_matrixBuffer)
		{
			m_matrixBuffer->Release();
			m_matrixBuffer = 0;
		}

		// Release the lighting constant buffer
		if (m_lightBuffer)
		{
			m_lightBuffer->Release();
			m_lightBuffer = 0;
		}

		// Release the tesselation constant buffer
		if (m_tesselationBuffer)
		{
			m_tesselationBuffer->Release();
			m_tesselationBuffer = 0;
		}

		// Release the layout.
		if (m_layout)
		{
			m_layout->Release();
			m_layout = 0;
		}

		//Release base shader components
		BaseShader::~BaseShader();
	}

	void TessellationShader::InitShader(WCHAR* vsFilename, WCHAR* psFilename)
	{
		D3D11_BUFFER_DESC matrixBufferDesc;
		D3D11_SAMPLER_DESC samplerDesc;

		// Load (+ compile) shader files
		loadVertexShader(vsFilename);
		loadPixelShader(psFilename);

		// Setup the description of the dynamic matrix constant buffer that is in the vertex shader.
		matrixBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
		matrixBufferDesc.ByteWidth = sizeof(MatrixBufferType);
		matrixBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		matrixBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		matrixBufferDesc.MiscFlags = 0;
		matrixBufferDesc.StructureByteStride = 0;

		// Create the constant buffer pointer so we can access the vertex shader constant buffer from within this class.
		m_device->CreateBuffer(&matrixBufferDesc, NULL, &m_matrixBuffer);

		// Create a texture sampler state description.
		samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
		samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_MIRROR;
		samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_MIRROR;
		samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_MIRROR;
		samplerDesc.MipLODBias = 0.0f;
		samplerDesc.MaxAnisotropy = 1;
		samplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
		samplerDesc.BorderColor[0] = 0;
		samplerDesc.BorderColor[1] = 0;
		samplerDesc.BorderColor[2] = 0;
		samplerDesc.BorderColor[3] = 0;
		samplerDesc.MinLOD = 0;
		samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;

		// Create the texture sampler state.
		m_device->CreateSamplerState(&samplerDesc, &m_sampleState);
	}

	void TessellationShader::InitShader(WCHAR* vsFilename, WCHAR* hsFilename, WCHAR* dsFilename, WCHAR* psFilename)
	{
		// InitShader must be overwritten and it will load both vertex and pixel shaders + setup buffers
		InitShader(vsFilename, psFilename);

		// Load other required shaders.
		loadHullShader(hsFilename);
		loadDomainShader(dsFilename);

		m_tesselationFactor = 64;
		m_cameraBasedTesselationDisabled = false;

		// ********************************************* TESSELATION BUFFER SET UP ***********************************************

		D3D11_BUFFER_DESC tesselationBufferDesc;
		// Setup the description of the tesselation constant buffer that is in the hull shader.
		tesselationBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
		tesselationBufferDesc.ByteWidth = sizeof(TesselationBufferType);
		tesselationBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		tesselationBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		tesselationBufferDesc.MiscFlags = 0;
		tesselationBufferDesc.StructureByteStride = 0;

		// Create the constant buffer pointer so we can access the vertex shader constant buffer from within this class.
		m_device->CreateBuffer(&tesselationBufferDesc, NULL, &m_tesselationBuffer);

		// ********************************************* LIGHT BUFFER SET UP ***********************************************

		D3D11_BUFFER_DESC lightBufferDesc;
		// Setup the description of the light constant buffer that is in the pixel shader.
		lightBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
		lightBufferDesc.ByteWidth = sizeof(LightBufferType3);
		lightBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		lightBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		lightBufferDesc.MiscFlags = 0;
		lightBufferDesc.StructureByteStride = 0;

		// Create the constant buffer pointer so we can access the vertex shader constant buffer from within this class.
		m_device->CreateBuffer(&lightBufferDesc, NULL, &m_lightBuffer);
	}

	void TessellationShader::Update(XMFLOAT3 cameraPos, XMFLOAT3 tesselatedMeshPos)
	{
		// If camera based tesselation is enabled
		if (!m_cameraBasedTesselationDisabled)
		{

			m_tesselationFactor = 64;

			// check x against far limit
			if (abs(cameraPos.x - tesselatedMeshPos.x) >= k_cameraLimitFar.x)
			{
				m_tesselationFactor = 16;
				return;
			}
			// check x against near limit
			else if (abs(cameraPos.x - tesselatedMeshPos.x) >= k_cameraLimitNear.x)
			{
				m_tesselationFactor = 32;
			}

			// check y against far limit
			if (abs(cameraPos.y - tesselatedMeshPos.y) >= k_cameraLimitFar.y)
			{
				m_tesselationFactor = 16;
				return;
			}
			// check y against near limit
			else if (abs(cameraPos.y - tesselatedMeshPos.y) >= k_cameraLimitNear.y)
			{
				m_tesselationFactor = 32;
			}

			// check z against far limit
			if (abs(cameraPos.z - tesselatedMeshPos.z) >= k_cameraLimitFar.z)
			{
				m_tesselationFactor = 16;
				return;
			}
			// check z against near limit
			else if (abs(cameraPos.z - tesselatedMeshPos.z) >= k_cameraLimitNear.z)
			{
				m_tesselationFactor = 32;
			}

			// SIMPLE DISTANCE CHECK DOES NOT ACCOUNT FOR THE LENGTH & WIDTH OF THE MESH
			//// get distance from camera to tesselatedmesh
			//float cameraToMeshDistance = CalcDistance(cameraPos, tesselatedMeshPos);
			//// set accordingly
			//if ((cameraToMeshDistance >= CAMERADISTANCE_NEAR) && (cameraToMeshDistance < CAMERADISTANCE_FAR))
			//	m_tesselationFactor = 32.f;
			//else if (cameraToMeshDistance >= CAMERADISTANCE_FAR)
			//	m_tesselationFactor = 16.f;
			//else
			//	m_tesselationFactor = 64.f;

		}
	}

	float TessellationShader::CalcDistance(XMFLOAT3 cameraPos, XMFLOAT3 tesselatedMeshPos)
	{
		float result;

		float xDiff = tesselatedMeshPos.x - cameraPos.x;
		float yDiff = tesselatedMeshPos.y - cameraPos.y;
		float zDiff = tesselatedMeshPos.z - cameraPos.z;

		float resultSq = xDiff * xDiff + yDiff * yDiff + zDiff * zDiff;

		result = sqrtf(resultSq);
		return result;
	}


	void TessellationShader::SetShaderParameters(ID3D11DeviceContext* deviceContext, const XMMATRIX &worldMatrix, const XMMATRIX &viewMatrix, const XMMATRIX &projectionMatrix,
		Light* light, ID3D11ShaderResourceView* texture, ID3D11ShaderResourceView* heightMap)
	{
		HRESULT result;
		D3D11_MAPPED_SUBRESOURCE mappedResource;
		MatrixBufferType* dataPtr;
		TesselationBufferType* tesselationPtr;
		LightBufferType3* lightPtr;
		unsigned int bufferNumber;
		XMMATRIX tworld, tview, tproj;

		// Transpose the matrices to prepare them for the shader.
		tworld = XMMatrixTranspose(worldMatrix);
		tview = XMMatrixTranspose(viewMatrix);
		tproj = XMMatrixTranspose(projectionMatrix);

		// ****************************** MATRIX BUFFER : DOMAIN SHADER *************************************

		// Lock the constant buffer so it can be written to.
		result = deviceContext->Map(m_matrixBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);

		// Get a pointer to the data in the constant buffer.
		dataPtr = (MatrixBufferType*)mappedResource.pData;

		// Copy the matrices into the constant buffer.
		dataPtr->world = tworld;// worldMatrix;
		dataPtr->view = tview;
		dataPtr->projection = tproj;

		// Unlock the constant buffer.
		deviceContext->Unmap(m_matrixBuffer, 0);

		// Set the position of the constant buffer in the vertex shader.
		bufferNumber = 0;

		// Now set the constant buffer in the domain shader with the updated values.
		deviceContext->DSSetConstantBuffers(bufferNumber, 1, &m_matrixBuffer);

		// ****************************** TESSELATION BUFFER : HULL SHADER **********************************

		// Lock the constant buffer so it can be written to.
		result = deviceContext->Map(m_tesselationBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);

		// Get a pointer to the data in the constant buffer.
		tesselationPtr = (TesselationBufferType*)mappedResource.pData;

		// Copy the data into the constant buffer.
		tesselationPtr->tesselationEdges.x = (float)m_tesselationFactor; //32.f;
		tesselationPtr->tesselationEdges.y = (float)m_tesselationFactor; //32.f;
		tesselationPtr->tesselationEdges.z = (float)m_tesselationFactor; //32.f;
		tesselationPtr->tesselationEdges.w = (float)m_tesselationFactor; //32.f;

		tesselationPtr->tesselationInside.x = (float)m_tesselationFactor; //32.f;
		tesselationPtr->tesselationInside.y = (float)m_tesselationFactor; //32.f;

		tesselationPtr->padding = XMFLOAT2(0.f, 0.f);

		// Unlock the constant buffer.
		deviceContext->Unmap(m_tesselationBuffer, 0);

		// Set the position of the constant buffer in the hull shader.
		bufferNumber = 0;

		// Now set the constant buffer in the hull shader with the updated values.
		deviceContext->HSSetConstantBuffers(bufferNumber, 1, &m_tesselationBuffer);

		// ****************************** LIGHT BUFFER : PIXEL SHADER **********************************

		// Lock the constant buffer so it can be written to.
		result = deviceContext->Map(m_lightBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);

		// Get a pointer to the data in the constant buffer.
		lightPtr = (LightBufferType3*)mappedResource.pData;

		// Copy the data into the constant buffer.
		lightPtr->ambientColour = light->GetAmbientColour();
		lightPtr->diffuseColour = light->GetDiffuseColour();

		// Unlock the constant buffer.
		deviceContext->Unmap(m_lightBuffer, 0);

		// Set the position of the constant buffer in the pixel shader.
		bufferNumber = 0;

		// Now set the constant buffer in the hull shader with the updated values.
		deviceContext->PSSetConstantBuffers(bufferNumber, 1, &m_lightBuffer);

		// *************************** SET RESOURCES *******************************************************

		// Set shader texture resource in the domain shader.
		deviceContext->DSSetShaderResources(0, 1, &heightMap);

		// Set shader texture resource in the pixel shader.
		deviceContext->PSSetShaderResources(0, 1, &texture);

	}

	void TessellationShader::Render(ID3D11DeviceContext* deviceContext, int indexCount)
	{
		// Set the sampler state in the pixel shader.
		deviceContext->PSSetSamplers(0, 1, &m_sampleState);
		deviceContext->DSSetSamplers(0, 1, &m_sampleState);


		// Base render function.
		BaseShader::Render(deviceContext, indexCount);
	}
}



