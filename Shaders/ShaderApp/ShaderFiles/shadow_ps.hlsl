
Texture2D shaderTexture : register(t0);
Texture2D depthMapTexture : register(t1);

SamplerState SampleTypeWrap  : register(s0);
SamplerState SampleTypeClamp : register(s1);

cbuffer LightBuffer : register(cb0)
{
	float4 ambientColour[4];
	float4 diffuseColour[4];
	// specular power is packed as the w component of position
	float4 position[4];
	float4 specularColour[4];
	// attenuation values are packed as follows:
	// constant, linear, quadratic, range
	float4 attenuationValues[4];
};

struct InputType
{
    float4 position : SV_POSITION;
    float2 tex : TEXCOORD0;
	float3 normal : NORMAL;
	float3 viewDirection : TEXCOORD1;
	float3 position3D : TEXCOORD2;
    float4 lightViewPosition : TEXCOORD3;
	float3 lightPos : TEXCOORD4;
};

// Shadows and basic lighting (ambient + diffuse) will be calculated using Light 0 using the shadow map texture
// Areas that fall outside the shadow map texture will be rendered with basic lighting (ambient + diffuse) 
// Full lighting is then calculated for lights 1 to 3, 'on top' of the lighting that has been calcualted for the shadows.

float4 main(InputType input) : SV_TARGET
{
	float bias;
    float4 colour;
	float2 projectTexCoord;
	float depthValue;
	float lightDepthValue;
    float lightIntensity;
	float4 textureColour;
	float4 finalSpec = float4(0.f, 0.f, 0.f, 1.f);
	
	// *************************** RENDER BASIC LIGHTING + SHADOWS BASED ON LIGHT 0 ONLY ************************************************************************

	// Set the bias value for fixing the floating point precision issues.
	bias = 0.0001f;

	// Set the default output color to the ambient light value for all pixels.
    colour = ambientColour[0];

	// Calculate the projected texture coordinates.
	projectTexCoord.x =  input.lightViewPosition.x / input.lightViewPosition.w / 2.0f + 0.5f;
	projectTexCoord.y = -input.lightViewPosition.y / input.lightViewPosition.w / 2.0f + 0.5f;

	// Determine if the projected coordinates are in the 0 to 1 range.  If so then this pixel is in the view of the light.
	if((saturate(projectTexCoord.x) == projectTexCoord.x) && (saturate(projectTexCoord.y) == projectTexCoord.y))
	{
		// Sample the shadow map depth value from the depth texture using the sampler at the projected texture coordinate location.
		depthValue = depthMapTexture.Sample(SampleTypeClamp, projectTexCoord).r;

		// Calculate the depth of the light.
		lightDepthValue = input.lightViewPosition.z / input.lightViewPosition.w;

		// Subtract the bias from the lightDepthValue.
		lightDepthValue = lightDepthValue - bias;

		// Compare the depth of the shadow map value and the depth of the light to determine whether to shadow or to light this pixel.
		// If the light is in front of the object then light the pixel, if not then shadow this pixel since an object (occluder) is casting a shadow on it.
		if(lightDepthValue < depthValue)
		{
		    // Calculate the amount of light on this pixel.
			lightIntensity = saturate(dot(input.normal, input.lightPos));

			if(lightIntensity > 0.0f)
			{
				// Determine the final diffuse color based on the diffuse color and the amount of light intensity.
				colour += (diffuseColour[0] * lightIntensity);

				// Saturate the final light color.
				colour = saturate(colour);
			}

		}
	}
	// otherwise, pixel is outside the bounds of the shadow map, render light normally
	else
	{
		// Calculate the amount of light on this pixel.
		lightIntensity = saturate(dot(input.normal, input.lightPos));

		if (lightIntensity > 0.0f)
		{
			// Determine the final diffuse color based on the diffuse color and the amount of light intensity.
			colour += (diffuseColour[0] * lightIntensity);

			// Saturate the final light color.
			colour = saturate(colour);
		}
	}

	// *************************** RENDER FULL LIGHTING USING LIGHTS 1 - 3 ************************************************************************
	for (int i = 1; i < 4; ++i)
	{

		// add the ambient light value to the colour.
		colour += ambientColour[i];

		// Invert the light direction for calculations.
		float3 light_pos = float3(position[i].x, position[i].y, position[i].z);	// this needs to be put into a float3 as it was packed as a float4 with the specularPower value
		float3 lightDir = input.position3D - light_pos;

		//	Calc distance
		float distance = length(lightDir);
		//	If distance < range
		if (distance < attenuationValues[i].w)
		{
			// Normalise light vector
			lightDir = normalize(lightDir);
			// Calculate the light intensity.
			float lightIntensity = saturate(dot(input.normal, -lightDir));

			// Calc diffuse intensity
			if (lightIntensity > 0.0f)
			{
				// Determine the final diffuse color based on the diffuse color and the amount 	of light intensity.
				float4 diffuse = (diffuseColour[i] * lightIntensity);
				// Calc attenuation value
				float attenuation = 1 / (attenuationValues[i].x + attenuationValues[i].y * distance + attenuationValues[i].z * pow(distance, 2));

				// If there were specular calculations to do too, they would go here
				// Calculate reflection vector based on the light intensity, normal vector and light direction
				float3 reflection = reflect(lightDir, input.normal);

				// Determine the amount of specular light based on the reflection vector, viewing direction, and specular power.
				float specularPower = position[i].w;											// unpack specular power from position
				float specular = pow(saturate(dot(reflection, input.viewDirection)), specularPower);

				//sum up specular light
				finalSpec += specularColour[i] * specular;
				// Color += diffuse comp * attenuation
				colour += diffuse * attenuation;

				// Saturate the ambient and diffuse color.
				colour = saturate(colour);

			}

		}

	}

	// Sample the pixel color from the texture using the sampler at this texture coordinate location.
	textureColour = shaderTexture.Sample(SampleTypeWrap, input.tex);

	// Combine the light and texture color.
	colour = colour * textureColour;

	// Add the specular component last to the output colour.
	colour = saturate(colour + finalSpec);

    return colour;
}