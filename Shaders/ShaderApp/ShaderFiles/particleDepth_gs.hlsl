// PARTICLE + DEPTH GEOMETRY SHADER

cbuffer MatrixBuffer : register(cb0)
{
    matrix worldMatrix;
    matrix viewMatrix;
    matrix projectionMatrix;
};

cbuffer ScaleBuffer : register(cb1)
{
	float particleScales[100];
}

cbuffer PositionBuffer : register(cb2)
{
	static float3 g_positions[4] =
	{
		float3(-0.25, 0.25f, 0),	// top left
		float3(-0.25f, -0.25f, 0),	// bottom left
		float3(0.25f, 0.25f, 0),	// top right
		float3(0.25f, -0.25, 0)	// bottom right 
	};
};

struct InputType
{
	float4 position : POSITION;
	float2 tex : TEXCOORD0;
	float3 normal : NORMAL;
	float index : PSIZE;
};

// pixel input type
struct OutputType
{
	float4 position : SV_POSITION;
	float4 depthPosition : TEXCOORD0;
};

// gs function
[maxvertexcount(4)]
void main(point InputType input[1], inout TriangleStream<OutputType> triStream)
{
	OutputType output;
		
	// Draw quads
	for (int i = 0; i < 4; i++)
	{
		// Change the position vector to be 4 units for proper matrix calculations.
		input[0].position.w = 1.0f;

		// get vertex position
		float3 vposition = g_positions[i];
		// calculate scaled vertex position
		vposition.x = vposition.x * particleScales[input[0].index];
		vposition.y = vposition.y * particleScales[input[0].index];

		// calc output position
		vposition = mul(vposition, (float3x3) worldMatrix) + input[0].position;
		// set output vertex position
		output.position = mul(float4(vposition, 1.0), viewMatrix);
		output.position = mul(output.position, projectionMatrix);

		output.depthPosition = output.position;

		triStream.Append(output);
	}

	// restart strip
	triStream.RestartStrip();

}