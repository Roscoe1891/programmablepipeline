// Light pixel shader
// Calculate diffuse lighting for a single directional light (also texturing)

Texture2D shaderTexture : register(t0);
SamplerState SampleType : register(s0);

cbuffer LightBuffer : register(cb0)
{
	float4 ambientColour[4];
	float4 diffuseColour[4];
	// specular power is packed as the w component of position
	float4 position[4];
	float4 specularColour[4];
	// attenuation values are packed as follows:
	// constant, linear, quadratic, range
	float4 attenuationValues[4];
};

struct InputType
{
	float4 position : SV_POSITION;
	float2 tex : TEXCOORD0;
	float3 normal : NORMAL;
	float3 viewDirection : TEXCOORD1;
	float3 position3D : TEXCOORD2;
};

float4 main(InputType input) : SV_TARGET
{
	float4 finalSpec = float4(0, 0, 0, 1);
	float4 colour = float4(0, 0, 0, 1);

	// Sample the pixel color from the texture using the sampler at this texture coordinate location.
	float4 textureColour = shaderTexture.Sample(SampleType, input.tex);

	for (int i = 0; i < 4; ++i)
	{
		
			// add the ambient light value to the colour.
			colour += ambientColour[i];

			// Invert the light direction for calculations.
			float3 light_pos = float3(position[i].x, position[i].y, position[i].z);	// this needs to be put into a float3 as it was packed as a float4 with the specularPower value
			float3 lightDir = input.position3D - light_pos;

			//	Calc distance
			float distance = length(lightDir);
			//	If distance < range
			if (distance < attenuationValues[i].w)
			{
				// Normalise light vector
				lightDir = normalize(lightDir);
				// Calculate the light intensity.
				float lightIntensity = saturate(dot(input.normal, -lightDir));

				// Calc diffuse intensity
				if (lightIntensity > 0.0f)
				{
					// Determine the final diffuse color based on the diffuse color and the amount 	of light intensity.
					float4 diffuse = (diffuseColour[i] * lightIntensity);
					// Calc attenuation value
					float attenuation = 1 / (attenuationValues[i].x + attenuationValues[i].y * distance + attenuationValues[i].z * pow(distance, 2));

					// If there were specular calculations to do too, they would go here
					// Calculate reflection vector based on the light intensity, normal vector and light direction
					float3 reflection = reflect(lightDir, input.normal);

					// Determine the amount of specular light based on the reflection vector, viewing direction, and specular power.
					float specularPower = position[i].w;											// unpack specular power from position
					float specular = pow(saturate(dot(reflection, input.viewDirection)), specularPower);

					//sum up specular light
					finalSpec += specularColour[i] * specular;
					// Color += diffuse comp * attenuation
					colour += diffuse * attenuation;

					// Saturate the ambient and diffuse color.
					colour = saturate(colour);

				}

			}
		
	}

	// Multiply the texture pixel and the final diffuse colour
	colour = colour * textureColour;

	// Add the specular component last to the output colour.
	colour = saturate(colour + finalSpec);

	return colour;

}



