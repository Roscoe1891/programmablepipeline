// PARTICLE VERTEX SHADER

cbuffer PositionBuffer : register (cb0)
{
	float4 particle_positions[100];
};

struct InputType
{
    float4 position : POSITION;
    float2 tex : TEXCOORD0;
    float3 normal : NORMAL;
};

struct OutputType
{
	float4 position : POSITION;
	float2 tex : TEXCOORD0;
	float3 normal : NORMAL;
	float index : PSIZE;				// lets us know which particle we are working with
};

OutputType main(InputType input)
{
	OutputType output;
	// positions have been replaced with index values, 
	// the actual positions are in the constant buffer
	// get index
	int positionIndex = input.position.x;
	// set output position
	output.position = particle_positions[positionIndex];
	// set output index
	output.index = positionIndex;

	// set other values
	output.tex = input.tex;
	output.normal = input.normal;

	return output;
}