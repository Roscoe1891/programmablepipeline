// Pixel shader to apply horizontal blur

Texture2D shaderTexture : register(t0);
Texture2D depthTexture : register(t1);
SamplerState SampleType : register(s0);

cbuffer BlurBuffer : register (cb0)
{
	float4 nearWeighting;
	float4 farWeighting;
};

struct InputType
{
    float4 position : SV_POSITION;
    float2 tex : TEXCOORD0;
    float2 texCoord1 : TEXCOORD1;
    float2 texCoord2 : TEXCOORD2;
    float2 texCoord3 : TEXCOORD3;
    float2 texCoord4 : TEXCOORD4;
    float2 texCoord5 : TEXCOORD5;
	float2 texCoord6 : TEXCOORD6;
	float2 texCoord7 : TEXCOORD7;
};

float4 main(InputType input) : SV_TARGET
{
	// define the near / far thresholds
	float far = 0.95;
	float near = 0.9;
	
	// Initialize the colour to black.
    float4 colour = float4(0.0f, 0.0f, 0.0f, 0.0f);

	// sample the depth of the centre texture
	float4 centreDepthSample = depthTexture.Sample(SampleType, input.texCoord4);

	// Add the nine horizontal pixels to the colour by the specific weight of each.
	if ((centreDepthSample.x >= near) && (centreDepthSample.x < far))
	{
		colour += shaderTexture.Sample(SampleType, input.texCoord1) * nearWeighting.w;
		colour += shaderTexture.Sample(SampleType, input.texCoord2) * nearWeighting.z;
		colour += shaderTexture.Sample(SampleType, input.texCoord3) * nearWeighting.y;
		colour += shaderTexture.Sample(SampleType, input.texCoord4) * nearWeighting.x;
		colour += shaderTexture.Sample(SampleType, input.texCoord5) * nearWeighting.y;
		colour += shaderTexture.Sample(SampleType, input.texCoord6) * nearWeighting.z;
		colour += shaderTexture.Sample(SampleType, input.texCoord7) * nearWeighting.w;

	}
	else if (centreDepthSample.x >= far)
	{
		colour += shaderTexture.Sample(SampleType, input.texCoord1) * farWeighting.w;
		colour += shaderTexture.Sample(SampleType, input.texCoord2) * farWeighting.z;
		colour += shaderTexture.Sample(SampleType, input.texCoord3) * farWeighting.y;
		colour += shaderTexture.Sample(SampleType, input.texCoord4) * farWeighting.x;
		colour += shaderTexture.Sample(SampleType, input.texCoord5) * farWeighting.y;
		colour += shaderTexture.Sample(SampleType, input.texCoord6) * farWeighting.z;
		colour += shaderTexture.Sample(SampleType, input.texCoord7) * farWeighting.w;
	}
	else
		colour += shaderTexture.Sample(SampleType, input.texCoord4);

	// Set the alpha channel to one.
    colour.a = 1.0f;

    return colour;
}


