// Tessellation Hull Shader
// Prepares control points for tessellation

cbuffer TesselationBuffer : register(cb0)
{
	float4 tesselationEdges;
	float2 tesselationInside;
	float2 padding;
}

struct InputType
{
	float3 position : POSITION;
	float2 tex : TEXCOORD0;
	float3 normal : NORMAL;
};

struct OutputType
{
	float3 position : POSITION;
	float2 tex : TEXCOORD0;
	float3 normal : NORMAL;
};

struct ConstantOutputType
{
    float edges[4] : SV_TessFactor;
    float inside[2] : SV_InsideTessFactor;
};



ConstantOutputType PatchConstantFunction(InputPatch<InputType, 4> inputPatch, uint patchId : SV_PrimitiveID)
{    
    ConstantOutputType output;

    // Set the tessellation factors for the four edges of the quad.
	output.edges[0] = tesselationEdges.x;
	output.edges[1] = tesselationEdges.y;
	output.edges[2] = tesselationEdges.z;
	output.edges[3] = tesselationEdges.w;

    // Set the tessellation factor for horizontal and vertical tessealtion inside the quad.
	output.inside[0] = tesselationInside.x;
	output.inside[1] = tesselationInside.y;

    return output;
}

[domain("quad")]
[partitioning("integer")]
[outputtopology("triangle_cw")]
[outputcontrolpoints(4)]
[patchconstantfunc("PatchConstantFunction")]
OutputType main(InputPatch<InputType, 4> patch, uint pointId : SV_OutputControlPointID, uint patchId : SV_PrimitiveID)
{
    OutputType output;

    // Set the position for this control point as the output position.
    output.position = patch[pointId].position;

    return output;
}