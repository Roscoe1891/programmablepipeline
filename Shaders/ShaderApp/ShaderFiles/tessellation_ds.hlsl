// Tessellation domain shader
// After tessellation the domain shader processes the all the vertices

Texture2D texture0 : register(t0);
SamplerState Sampler0 : register(s0);

cbuffer MatrixBuffer : register(cb0)
{
    matrix worldMatrix;
    matrix viewMatrix;
    matrix projectionMatrix;
};

struct ConstantOutputType
{
    float edges[4] : SV_TessFactor;
    float inside[2] : SV_InsideTessFactor;
};

struct InputType
{
	float3 position : POSITION;
	float2 tex : TEXCOORD0;
	float3 normal : NORMAL;
};

struct OutputType
{
	float4 position : SV_POSITION;
	float2 tex : TEXCOORD0;
	float3 normal : NORMAL;
};

[domain("quad")]
OutputType main(ConstantOutputType input, float2 uvCoord : SV_DomainLocation, const OutputPatch<InputType, 4> patch)
{
    OutputType output;
 
	float3 vertexPosition;
	// Determine the position of the new vertex by bilinear interpolation.
	float3 v1 = lerp(patch[0].position, patch[1].position, 1 - uvCoord.y);
	float3 v2 = lerp(patch[2].position, patch[3].position, 1 - uvCoord.y);
	vertexPosition = lerp(v1, v2, uvCoord.x);

	// Determine the position of the new vertex by bilinear interpolation.
	float2 texCoords = lerp(float2(v1.x, v1.y), float2(v2.x, v2.y), uvCoord.x);
	// Offset to be between 0 & 1
	texCoords.x = texCoords.x / 2 - 0.5;
	texCoords.y = texCoords.y / 2 - 0.5;
    
	// VERTEX MANIPULATION : Z AXIS FROM TEXTURE MAP
	// Sample the pixel color from the texture using the sampler at this texture coordinate location.
	float4 textureSample = texture0.SampleLevel(Sampler0, texCoords, 0);
	vertexPosition.z = textureSample.z;

    // Calculate the position of the new vertex against the world, view, and projection matrices.
    output.position = mul(float4(vertexPosition, 1.0f), worldMatrix);
    output.position = mul(output.position, viewMatrix);
    output.position = mul(output.position, projectionMatrix);

	output.tex = texCoords;
  
    return output;
}

