// Tessellation pixel shader
// Output colour passed to stage.

Texture2D texture0 : register(t0);
SamplerState Sampler0 : register(s0);

cbuffer LightBuffer : register(cb0)
{
	float4 ambientColour;
	float4 diffuseColour;
};

struct InputType
{
	float4 position : SV_POSITION;
	float2 tex : TEXCOORD0;
	float3 normal : NORMAL;
};

float4 main(InputType input) : SV_TARGET
{
	// simplified lighting:
	// Only and always apply ambient and diffuse lighting from the sun
	float4 colour = float4(0.f, 0.f, 0.f, 0.f);

	// Sample the pixel color from the texture using the sampler at this texture coordinate location.
	float4 textureColour = texture0.Sample(Sampler0, input.tex);

	// add the ambient light value to the colour.
	colour += ambientColour;
	colour += diffuseColour;

	// Multiply the texture pixel and the final diffuse colour
	colour = colour * textureColour;

	// Saturate the ambient and diffuse color.
	colour = saturate(colour);

	return colour;
}