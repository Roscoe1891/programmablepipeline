// Application.h
#ifndef _APP1_H
#define _APP1_H

// Includes
#include "../DXFramework/baseapplication.h"
#include "D3D.h"
#include "../DXFramework/CubeMesh.h"
#include "../DXFramework/OrthoMesh.h"
#include "../DXFramework/ParticleSystemMesh.h"
#include "../DXFramework/PlaneMesh.h"
#include "../DXFramework/PointMesh.h"
#include "../DXFramework/QuadMesh.h"
#include "../DXFramework/RenderTexture.h"
#include "../DXFramework/SphereMesh.h"
#include "../DXFramework/TessellationTriMesh.h"
#include "../DXFramework/TessellationQuadMesh.h"
#include "../DXFramework/Model.h"

#include <Lights/LightManager.h>
#include <Scene/ParticleSystem.h>
#include <Scene/Terrain.h>
#include <Scene/SceneObject.h>
#include <Shaders/ShaderManager.h>
#include <Utilities/MathsUtilities.h>
#include <GUI/GUI.h>

using namespace ShaderApp;
using namespace DirectX;

class App1 : public BaseApplication
{
public:
	App1();
	~App1();
	void init(HINSTANCE hinstance, HWND hwnd, int screenWidth, int screenHeight, Input*);
	bool Frame();

protected:
	void CleanUp();
	bool Render();
	bool RenderToTextureShadow();			// STAGE 1 : GET A DEPTH MAP USED FOR SHADOWS
	bool RenderToTextureDepth();			// STAGE 2 : GET A DEPTH MAP FROM THE CAMERA TO CALCULATE BLUR LATER 
	bool RenderToTexture();					// STAGE 3 : RENDER SCENE (WITH SHADOWS) TO TEXTURE
	bool BlurHorizontal();					// STAGE 4 : BLUR (USING DEPTH MAP) HORIZONTALLY  
	bool BlurVertical();					// STAGE 5 : BLUR (USING DEPTH MAP) VERTICALLY
	bool RenderPostProcess();				// STAGE 6 : RENDER FINAL POST PROCESS IMAGE
	bool RenderNoBlur();

private:
	void CreateRenderTextures();
	void CreateMeshes();
	void InitSceneObjects();

	// Lights
	LightManager m_LightManager;
	
	// Shaders
	ShaderManager m_ShaderManager;
	
	// GUI
	GUI m_GUI;

	// RENDER TEXTURES
	// Shadow map
	RenderTexture* m_RenderTextureShadow;
	// Depth map
	RenderTexture* m_RenderTextureDepth;
	// Scene
	RenderTexture* m_RenderTextureScene;
	// Horizontally blurred
	RenderTexture* m_RenderTextureHorizontalBlur;
	// Vertically blurred
	RenderTexture* m_RenderTextureVerticalBlur;
	
	// ORTHOMESHES FOR DRAWING RENDER TEXTURES
	// Draw Shadow Map
	OrthoMesh* m_OrthoMeshShadow;
	// Draw Depth Map
	OrthoMesh* m_OrthoMeshDepth;
	// Draw Scene
	OrthoMesh* m_OrthoMeshScene;
	// Draw horinzontal blur
	OrthoMesh* m_OrthoMeshHorizontalBlur;
	// Draw Vertical blur
	OrthoMesh* m_OrthoMeshVerticalBlur;
	// Draw final image
	OrthoMesh* m_OrthoMeshPostProcessed;

	// horizontal blur full window mesh
	OrthoMesh* m_OrthoMeshHorizontalBlurFullWindow;
	// vertical blur full window mesh
	OrthoMesh* m_OrthoMeshVerticalBlurFullWindow;

	// MESHES USED FOR SCENE OBJECTS
	CubeMesh* m_CubeMesh;
	PlaneMesh* m_PlaneMesh;
	//SphereMesh* m_SphereMesh;
	ParticleSystemMesh* m_ParticleSystemMesh;
	TessellationQuadMesh* m_TesselationMesh;
	Model* m_diamondModel;
	Model* m_pillarModel;

	// SCENE OBJECTS
	SceneObject m_diamond;
	SceneObject m_pillar1;
	SceneObject m_pillar2;
	SceneObject m_platform;
	SceneObject m_liquid_plane;
	Terrain m_Terrain;
	ParticleSystem m_ParticleSystem;

	// Scene Objects used as draw objects for the rendering process
	SceneObject m_windowHorizontalBlur;
	SceneObject m_windowVerticalBlur;
	SceneObject m_finalWindow;

	// Debug Objects
	//SceneObject m_debugLightMarker;
	SceneObject m_debugWindowShadow;
	SceneObject m_debugWindowDepth;
	SceneObject m_debugWindowScene;
	SceneObject m_debugWindowHorizontalBlur;
	SceneObject m_debugWindowVerticalBlur;

};

#endif